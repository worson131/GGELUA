#ifndef gge_h
#define gge_h
#if defined(_WIN32)
#include <stdio.h>
#include <wchar.h>
#include <windows.h>

FILE* utf8_fopen(char const* _FileName, char const* _Mode);
FILE* utf8_freopen(const char* _FileName, const char* _Mode, FILE* _OldStream);
int utf8_system(const char* _Command);
const char* utf8_getenv(char const* _VarName);
int utf8_remove(const char* _FileName);
int utf8_rename(char const* _OldFileName, char const* _NewFileName);

#define fopen	utf8_fopen
#define freopen utf8_freopen
#define system utf8_system//execute
#define getenv utf8_getenv
#define remove utf8_remove
#define rename utf8_rename
#endif
#endif