#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "SDL_stdinc.h"
#include "SDL_mutex.h"
#include "SDL_messagebox.h"
#include "SDL_rwops.h"
#include "SDL_main.h"
#include <windows.h>
struct INFO {
	unsigned int signal;
	unsigned int coresize;
	unsigned int packsize;
};
char* ggelua = NULL;
char* ggepack = NULL;
struct INFO info={0,0,0};
#if defined(__WIN32__)
static int GGE_LoadWindows(lua_State *L)
{
	size_t l;
	const char* path = lua_tolstring(L,1,&l);
	size_t i;
	SDL_RWops*f = SDL_RWFromFile(path,"rb");
	if (f==NULL) 
		return 0;
	if (SDL_RWseek(f,-(int)sizeof(info), RW_SEEK_END)==0){
		SDL_FreeRW(f);
		return 0;
	}
	if (SDL_RWread(f,&info,sizeof(info),1)==0){
		SDL_FreeRW(f);
		return 0;
	}
	if (info.signal==0x20454747){
		if (SDL_RWseek(f,-(int)(sizeof(info)+info.coresize+info.packsize), RW_SEEK_END)==0){
			SDL_FreeRW(f);
			return 0;
		}
		ggelua = (char*)SDL_malloc(info.coresize);
		SDL_RWread(f,ggelua,info.coresize,1);
		ggepack = (char*)SDL_malloc(info.packsize);
		SDL_RWread(f,ggepack,info.packsize,1);
	}else{
		char lpath[256];
		for (i=0;i<l;i++)
			lpath[i] = SDL_tolower(path[i]);
		lpath[l] = 0;
#ifdef _CONSOLE
		path = luaL_gsub(L, lpath, "ggeluac.exe", "ggelua.lua");
#else
		path = luaL_gsub(L, lpath, "ggelua.exe", "ggelua.lua");
#endif
		ggelua = (char*)SDL_LoadFile(path, &info.coresize);
		info.packsize = 0;
	}
	SDL_FreeRW(f);
	return 0;
}
#endif
static int GGE_Error(lua_State* L) {
#ifdef _CONSOLE
	printf("%s\n", lua_tostring(L, -1));
#else
	lua_getfield(L, LUA_REGISTRYINDEX, LUA_LOADED_TABLE);//LOADED
	if (lua_getfield(L,-1,"SDL")!= LUA_TTABLE){
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "error", lua_tostring(L, -3), NULL);
	}else{
		printf("%s\n", lua_tostring(L, -3));
	}
#endif
	return 0;
}

int SDL_main(int argc, char* argv[])
{
#ifdef _CONSOLE
    SetConsoleOutputCP(65001);
#endif
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);

	SDL_mutex **extra = (SDL_mutex **)lua_getextraspace(L);//线程锁
	SDL_mutex*mutex = SDL_CreateMutex();
	*extra = mutex;

    lua_createtable(L, argc, 0);
    for (int i = 1; i < argc; i++) {
        lua_pushstring(L, argv[i]);
        lua_seti(L, -2, i);
    }
	lua_setglobal(L, "arg");
#if defined(__WIN32__)
	lua_pushcfunction(L,GGE_LoadWindows);//读取脚本
	lua_pushstring(L, argv[0]);
	lua_call(L, 1, 0);
#endif /* __WIN32__ */
	if (ggelua){
		lua_pushlstring(L, ggelua, info.coresize);
		lua_setfield(L, LUA_REGISTRYINDEX, "ggelua.lua");
		if (ggepack) {
			lua_pushlstring(L, ggepack, info.packsize);
			lua_setfield(L, LUA_REGISTRYINDEX, "ggepack");
		}
		SDL_LockMutex(mutex);//ggelua.delay解锁

		if (luaL_loadbuffer(L,ggelua,info.coresize,"ggelua.lua")== LUA_OK){
			lua_pushstring(L,"main");
			if (ggepack)//打包的脚本数据
				lua_pushlstring(L,ggepack,info.packsize);
			else
				lua_pushnil(L);
			if (lua_pcall(L, 2, 0, 0)!= LUA_OK){
				GGE_Error(L);
			}
		}else
			GGE_Error(L);
		
		SDL_free(ggelua);
	}
	
	SDL_DestroyMutex(mutex);
	lua_close(L);

	if (ggepack)
		SDL_free(ggepack);
	return 0;
}