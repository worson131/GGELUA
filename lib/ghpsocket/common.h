#pragma once
#include "lua.hpp"
#include "HPSocket.h"

#define CASE(X) case X: lua_pushstring(L,#X);break;
//定义自己的互斥
#include "SDL_mutex.h"
#define HP_mutex SDL_mutex
#define HP_LockMutex SDL_LockMutex
#define HP_UnlockMutex SDL_UnlockMutex

class HP_Base
{
public:
	HP_Base(lua_State* L){
		m_self = luaL_ref(L,LUA_REGISTRYINDEX);
		m_mutex = *(HP_mutex **)lua_getextraspace(L);
		m_L = L;
	};

	~HP_Base(){
		luaL_unref(m_L,LUA_REGISTRYINDEX,m_self);
	};

	void GetCallBack(const char* name){
		HP_LockMutex(m_mutex);
		L = lua_newthread(m_L);
		lua_rawgeti(L, LUA_REGISTRYINDEX, m_self);
		lua_getfield(L,-1,name);
		lua_insert(L,1);//self
	};

	EnHandleResult GetResult(){
		Uint32 r = (Uint32)lua_tointeger(L,-1);
		lua_pop(m_L,1);//协程
		HP_UnlockMutex(m_mutex);
		return EnHandleResult(r);
	};

	EnHttpParseResult GetResult2(){
		Uint32 r = (Uint32)lua_tointeger(L,-1);
		lua_pop(m_L,1);//协程
		HP_UnlockMutex(m_mutex);
		return EnHttpParseResult(r);
	};

	HP_mutex *m_mutex;
	lua_State* m_L;//主状态机
	int m_self;
	lua_State* L;//协程
};

//ITcpClient
int SendSmallFile(lua_State* L);
int SetSocketBufferSize(lua_State* L);
int SetKeepAliveTime(lua_State* L);
int SetKeepAliveInterval(lua_State* L);
int GetSocketBufferSize(lua_State* L);
int GetKeepAliveTime(lua_State* L);
int GetKeepAliveInterval(lua_State* L);
int Start(lua_State* L);
int Stop(lua_State* L);
int Send(lua_State* L);
int SendPackets(lua_State* L);
int PauseReceive(lua_State* L);
int Wait(lua_State* L);
int SetExtra(lua_State* L);
int GetExtra(lua_State* L);
int IsSecure(lua_State* L);
int HasStarted(lua_State* L);
int GetState(lua_State* L);
int GetLastError(lua_State* L);
int GetLastErrorDesc(lua_State* L);
int GetConnectionID(lua_State* L);
int GetLocalAddress(lua_State* L);
int GetRemoteHost(lua_State* L);
int GetPendingDataLength(lua_State* L);
int IsPauseReceive(lua_State* L);
int IsConnected(lua_State* L);
int SetReuseAddressPolicy(lua_State* L);
int SetFreeBufferPoolSize(lua_State* L);
int SetFreeBufferPoolHold(lua_State* L);
int GetReuseAddressPolicy(lua_State* L);
int GetFreeBufferPoolSize(lua_State* L);
int GetFreeBufferPoolHold(lua_State* L);