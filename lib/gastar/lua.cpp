#include "lua\lua.hpp"
#include "Astart.h"

static int astar_CheckPoint(lua_State *L)
{
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	int x = luaL_checkinteger(L,2);
	int y = luaL_checkinteger(L,3);

	if (map->数据 && x>=0 && x<map->宽度 && y>=0 && y<map->高度){
		int n = y*map->宽度+x;
		lua_pushboolean(L,map->数据[n]==0);
	}else
		lua_pushboolean(L,0);

	return 1;
}

static int astar_GetPoint(lua_State *L)
{
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	int x = luaL_checkinteger(L,2);
	int y = luaL_checkinteger(L,3);

	if (map->数据 && x>=0 && x<map->宽度 && y>=0 && y<map->高度){
		int n = y*map->宽度+x;
		lua_pushinteger(L,map->数据[n]);
	}else
		lua_pushinteger(L,-1);

	return 1;
}

static int astar_SetPoint(lua_State *L)
{
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	int x = luaL_checkinteger(L,2);
	int y = luaL_checkinteger(L,3);
	int v = luaL_checkinteger(L,4);

	if (map->数据 && x>=0 && x<map->宽度 && y>=0 && y<map->高度)
	{
		int n = y*map->宽度+x;
		map->数据[n] = v;
	}
	return 0;
}

static int astar_GetPath(lua_State *L)
{
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	POINT nodeStart,nodeEnd,nodeCur;
	nodeStart.x = luaL_checkinteger(L, 2);
	nodeStart.y = luaL_checkinteger(L, 3);
	nodeEnd.x   = luaL_checkinteger(L, 4);
	nodeEnd.y   = luaL_checkinteger(L, 5);
	BOOL mode	= luaL_optinteger(L,6,0);
	nodeCur = nodeStart;

	lua_newtable(L);
	if (nodeEnd.x<0 || nodeEnd.x>=map->宽度 || nodeEnd.y<0 || nodeEnd.y>=map->高度)
		return 1;
	if (FindPath(map, &nodeStart, &nodeEnd,mode))
	{
		int i = 1;
		for( ;; )
		{
			if (NextPath(map,&nodeCur))
			{
				lua_newtable(L);
				lua_pushinteger(L, nodeCur.x);
				lua_setfield(L,-2,"x");
				lua_pushinteger(L, nodeCur.y);
				lua_setfield(L,-2,"y");
				lua_rawseti(L, -2, i++);
				if( nodeCur.x==nodeEnd.x && nodeCur.y==nodeEnd.y ) break;
			}else
				break;
		};
	}
	return 1;
}

static int astar_new(lua_State *L)
{
	int w,h;
	char* data;
	size_t len;
	w = luaL_checkinteger(L, 1);
	h = luaL_checkinteger(L, 2);
	地图结构* map = (地图结构*)lua_newuserdata(L,sizeof(地图结构));
	MapCreate2(map,w,h,NULL);
	map->数据 = new BYTE[map->大小];
	memset(map->数据,0,map->大小);
	if (lua_isnumber(L,3) && lua_isnumber(L,4)){
		data = (char*)luaL_checkinteger(L, 3);
		len  = luaL_checkinteger(L, 4);
		if (len<=map->大小){
			memcpy(map->数据,data,len);
		}else
			luaL_error(L,"data error");
	}else if (lua_isstring(L,3)){
		data = (char*)luaL_checklstring(L,3,&len);
		if(len==map->大小)
			memcpy(map->数据,data,len);
	}
	luaL_getmetatable(L, "gge_astar");
	lua_setmetatable(L, -2);
	return 1;
}

static int astar_gc(lua_State *L)
{
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	if (map->数据)
	{
		delete []map->数据;
		MapDestroy2(map);
		map->数据 = NULL;
	}

	return 0;
}
extern "C"
LUALIB_API int luaopen_gastar(lua_State* L)
{
	luaL_Reg methods[] = {
		{"__gc", astar_gc},
		{"GetPath", astar_GetPath},
		{"CheckPoint", astar_CheckPoint},
		{"GetPoint", astar_GetPoint},
		{"SetPoint", astar_SetPoint},
		{NULL, NULL},
	};
	luaL_newmetatable(L, "gge_astar");
	luaL_setfuncs(L,methods,0);
	lua_pushvalue(L,-1);
	lua_setfield(L, -2, "__index");
	lua_pop(L,1);

	lua_pushcfunction(L,astar_new);
	return 1;
}

