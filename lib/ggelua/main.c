#include <Windows.h>
#include "luaopen.h"
#include "SDL_clipboard.h"
#include "SDL_filesystem.h"
#include "SDL_messagebox.h"
#include "SDL_mutex.h"
#include "SDL_platform.h"
#include "SDL_rect.h"
#include "SDL_stdinc.h"
#include "SDL_thread.h"
#include "SDL_timer.h"

static int LUA_ANSI2UTF8(lua_State *L){
	const char* str = luaL_checkstring(L,1);
#ifdef __WIN32__
	luaL_Buffer b;
	WCHAR* temp ; 
	char* p ;
	int len = MultiByteToWideChar(CP_ACP, 0, str, -1, NULL, 0);
	temp = (WCHAR *)malloc(len*sizeof(WCHAR));
	MultiByteToWideChar (CP_ACP, 0, str, -1, temp, len);

	len = WideCharToMultiByte(CP_UTF8,0,temp,-1,NULL,0,NULL,0);
	p = luaL_buffinitsize(L,&b,len);
	len = WideCharToMultiByte(CP_UTF8,0,temp,-1,p,len,NULL,0);

	free(temp);
	luaL_pushresultsize(&b,len-1);
#endif
	return 1;
}

static int LUA_UTF82ANSI(lua_State *L){
	const char* str = luaL_checkstring(L,1);
#ifdef __WIN32__
	luaL_Buffer b;
	WCHAR* temp ; 
	char* p ;
	int len = MultiByteToWideChar(CP_UTF8, 0, str,-1, NULL, 0);
	temp = (WCHAR *)malloc(len*sizeof(WCHAR));
	MultiByteToWideChar (CP_UTF8, 0, str, -1, temp, len);

	len = WideCharToMultiByte(CP_ACP,0,temp,-1,NULL,0,NULL,0);
	p = luaL_buffinitsize(L,&b,len);
	len = WideCharToMultiByte(CP_ACP,0,temp,-1,p,len,NULL,0);

	free(temp);
	luaL_pushresultsize(&b,len-1);
#endif
	return 1;
}

static int LUA_GetRunPath(lua_State *L) {
	luaL_Buffer b;
	char *p;
	DWORD n;
	p = luaL_buffinitsize(L,&b, 256);
	n = GetModuleFileName(NULL, p, 256);
	p[n] = 0;
	luaL_pushresultsize(&b,strrchr(p, '\\')-p);  /* close buffer */
	LUA_ANSI2UTF8(L);
	return 1;
}

static int LUA_GetCurPath(lua_State *L) {
	luaL_Buffer b;
	char* p;
	DWORD n;
	p = luaL_buffinitsize(L, &b, 256);
	n = GetCurrentDirectory(LUAL_BUFFERSIZE, p);
	luaL_pushresultsize(&b,n);  /* close buffer */
	LUA_ANSI2UTF8(L);
	return 1;
}

static int LUA_SetCurPath(lua_State *L) {
	const char* str;
	LUA_UTF82ANSI(L);
	str = luaL_checkstring(L,-1);

	lua_pushboolean(L,SetCurrentDirectory(str));
	return 1;
}

char* ggelua = NULL;//启动脚本
size_t coresize;
char* ggepack = NULL;//打包脚本
size_t packsize;
static int LUA_StateThread(void *data){
	lua_State *L = luaL_newstate();
	SDL_mutex **extra = (SDL_mutex **)lua_getextraspace(L);
	SDL_mutex *mutex = SDL_CreateMutex();
	*extra = mutex;
	
	luaL_openlibs(L);

	SDL_LockMutex(mutex);
	if (ggelua && luaL_loadbuffer(L,ggelua,coresize,"ggelua.lua")== LUA_OK){
		lua_pushstring(L,(char*)data);
		if (ggepack)
			lua_pushlstring(L,ggepack,packsize);
		else
			lua_pushnil(L);
		lua_pushboolean(L,1);//是否虚拟机
		if (lua_pcall(L, 3, 0, 0)!= LUA_OK){
			printf("%s\n",lua_tostring(L,-1));
		}
	}
	
	SDL_DestroyMutex(mutex);
	lua_close(L);
	return 0;
}
//SDL
static int LUA_NewState(lua_State *L) {
	const char*init = luaL_checkstring(L,1);
	if (lua_getfield(L, LUA_REGISTRYINDEX, "ggelua.lua") == LUA_TSTRING){
		ggelua = (char*)lua_tolstring(L, -1, &coresize);
		if (lua_getfield(L, LUA_REGISTRYINDEX, "ggepack") == LUA_TSTRING)
			ggepack = (char*)lua_tolstring(L, -1, &packsize);
		SDL_Thread* t = SDL_CreateThread(LUA_StateThread, NULL, (void*)init);
		lua_pushboolean(L, t != NULL);
	}else
		lua_pushboolean(L, 0);

	return 1;
}

static int LUA_Delay(lua_State *L) {
	Uint32 n = (Uint32)lua_tointeger(L,1);
	SDL_mutex *mutex = *(SDL_mutex **)lua_getextraspace(L);
	SDL_UnlockMutex(mutex);
	SDL_Delay(n);
	SDL_LockMutex(mutex);
	return 0;
}

static int	LUA_SetClipboardText(lua_State *L){
	const char *text = luaL_checkstring(L, 1);
	lua_pushboolean(L,SDL_SetClipboardText(text) == 0);
	return 1;
}

static int	LUA_GetClipboardText(lua_State *L){
	char *str = SDL_GetClipboardText();
	lua_pushstring(L, str);
	SDL_free(str);
	return 1;
}

static int	LUA_HasClipboardText(lua_State *L){
	lua_pushboolean(L,SDL_HasClipboardText());
	return 1;
}

static int	LUA_GetBasePath(lua_State *L){
	char *str = SDL_GetBasePath();
	lua_pushstring(L, str);
	SDL_free(str);
	return 1;

}
//%AppData%\Roaming\GGELUA
static int	LUA_GetPrefPath(lua_State *L){
	//const char *organization = luaL_checkstring(L, 1);
	const char *application = luaL_checkstring(L, 1);
	char *str = SDL_GetPrefPath("GGELUA", application);

	lua_pushstring(L, str);
	SDL_free(str);
	return 1;

}

static int LUA_MessageBox(lua_State *L) {
	const char *message = luaL_optstring(L,1,"");
	const char *title = luaL_optstring(L,2,"");
	Uint32 flags = (int)luaL_optinteger(L,3,0);

	lua_pushboolean(L,SDL_ShowSimpleMessageBox(flags,title,message,NULL)==0);
	return 1;
}

static int LUA_GetPlatform(lua_State *L){
	lua_pushstring(L,SDL_GetPlatform());
	return 1;
}

static const luaL_Reg fun_list[] = {
	{"ansitoutf8"     , LUA_ANSI2UTF8}      ,
	{"utf8toansi"     , LUA_UTF82ANSI}      ,
	{"getcurpath"     , LUA_GetCurPath}     ,
	{"setcurpath"     , LUA_SetCurPath}     ,
	{"getrunpath"     , LUA_GetRunPath}     ,
	{"newstate"       , LUA_NewState}     ,

	{"delay"   , LUA_Delay} ,
	{"messagebox"     , LUA_MessageBox}  ,
	{"setclipboardtext", LUA_SetClipboardText},
	{"getclipboardtext", LUA_GetClipboardText},
	{"hasclipboardtext", LUA_HasClipboardText},
	{"getbasepath", LUA_GetBasePath},
	{"getprefpath", LUA_GetPrefPath},
	{"getplatform", LUA_GetPlatform},

	{"isconsole"        , NULL} ,
	{NULL             , NULL}
};

static const luaL_Reg lib_list[] = {
	{"zlib",   luaopen_zlib},
	{"md5",    luaopen_md5},
	{"base64", luaopen_base64},
	{"cmsgpack", luaopen_cmsgpack},
	{"cmsgpack.safe", luaopen_cmsgpack_safe},
	{"lfs", luaopen_lfs},
	{"uuid", luaopen_uuid},
	{"cprint",luaopen_cprint},
	{NULL, NULL},
};


LUALIB_API int luaopen_ggelua(lua_State *L) {
	lua_pushstring(L, "GGE 1.0");
	lua_setglobal(L, "_GGEVERSION");
	//内置模块
	lua_getfield(L, LUA_REGISTRYINDEX, LUA_PRELOAD_TABLE);//PRELOAD
	luaL_setfuncs(L, lib_list,0);
	lua_pop(L, 1);  // remove PRELOAD

	//内置函数
	luaL_newlib(L, fun_list);
#ifdef __WIN32__
	lua_pushboolean(L, GetConsoleOutputCP() == 65001);
	lua_setfield(L, -2, "isconsole");
#endif
	//package.loaded.ggelua
	lua_getfield(L, LUA_REGISTRYINDEX, LUA_LOADED_TABLE);//LOADED
	lua_pushvalue(L, -2);
	lua_setfield(L, -2, "ggelua");//LOADED.ggelua
	lua_pop(L, 1);  // remove LOADED
	return 1;//fun_list
}