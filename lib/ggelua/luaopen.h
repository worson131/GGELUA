#pragma once
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
int luaopen_des56(lua_State* L);
int luaopen_zlib(lua_State* L);
int luaopen_md5(lua_State* L);
int luaopen_base64(lua_State* L);
int luaopen_cmsgpack(lua_State* L);
int luaopen_cmsgpack_safe(lua_State* L);
int luaopen_lfs(lua_State* L);
int luaopen_uuid(lua_State* L);
int luaopen_cprint(lua_State* L);