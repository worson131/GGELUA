#include "gge.h"
#include <SDL_image.h>
#include <SDL_syswm.h>
#include <Windows.h>
//Texture取像素
int GGE_GetTexturePixel(lua_State *L)
{
    GGE_Texture * tex = (GGE_Texture*)luaL_checkudata(L, 1, "SDL_Texture");
    int x = (int)luaL_checkinteger(L,2);
    int y = (int)luaL_checkinteger(L,3);
    
    int format,access, w, h;
    if (!tex->rect && SDL_QueryTexture(tex->tex , &format, &access, &w, &h)==0 && access==SDL_TEXTUREACCESS_STREAMING && x>=0 && y>=0 && x<w && y<h){
        if(!tex->locked){
            SDL_LockTexture(tex->tex,NULL,&tex->pixels,&tex->pitch);
            tex->locked = 1;
        }
        if (!tex->format)
            tex->format = SDL_AllocFormat(format);
        {
            Uint8 r,g,b,a;
            int bpp = tex->format->BytesPerPixel;
            Uint8 *p = (Uint8 *)tex->pixels + y * tex->pitch + x * bpp;
            switch (bpp) {
            case 2:{
                SDL_GetRGBA(*(Uint16 *)p,tex->format,&r,&g,&b,&a);
                lua_pushinteger(L,a);
                lua_pushinteger(L,r);
                lua_pushinteger(L,g);
                lua_pushinteger(L,b);
                }
            case 3:{}
            case 4:{
                SDL_GetRGBA(*(Uint32 *)p,tex->format,&r,&g,&b,&a);
                lua_pushinteger(L,a);
                lua_pushinteger(L,r);
                lua_pushinteger(L,g);
                lua_pushinteger(L,b);
                }
            }
        }
    }else{
        lua_pushinteger(L,0);
        lua_pushinteger(L,0);
        lua_pushinteger(L,0);
        lua_pushinteger(L,0);
    }
    return 4;
}
//Texture置像素
int LUA_SetTexturePixel(lua_State *L)
{
    GGE_Texture * tex = (GGE_Texture*)luaL_checkudata(L, 1, "SDL_Texture");
    int x = (int)luaL_checkinteger(L,2);
    int y = (int)luaL_checkinteger(L,3);
    int r = (int)luaL_optinteger(L,4,0);
    int g = (int)luaL_optinteger(L,5,0);
    int b = (int)luaL_optinteger(L,6,0);
    int a = (int)luaL_optinteger(L,7,0);
    int format,access, w, h;

    if (!tex->rect && SDL_QueryTexture(tex->tex , &format, &access, &w, &h)==0 && access==SDL_TEXTUREACCESS_STREAMING && x>=0 && y>=0 && x<w && y<h){
        if(!tex->locked){
            SDL_LockTexture(tex->tex,NULL,&tex->pixels,&tex->pitch);
            tex->locked = 1;
        }
        if (!tex->format)
            tex->format = SDL_AllocFormat(format);

        {
            int bpp = tex->format->BytesPerPixel;
            Uint8 *p = (Uint8 *)tex->pixels + y * tex->pitch + x * bpp;
            switch (bpp) {
            case 2:
                *(Uint16 *)p = SDL_MapRGBA(tex->format,r,g,b,a);
            case 3:{}
            case 4:
                *(Uint32 *)p = SDL_MapRGBA(tex->format,r,g,b,a);
            }
        }
    }

    return 0;
}
//Texture灰度
int GGE_TextureToGrayscale(lua_State *L)
{
    GGE_Texture * tex = (GGE_Texture*)luaL_checkudata(L, 1, "SDL_Texture");
    int access,format, w, h;
    
    if (!tex->rect && SDL_QueryTexture(tex->tex , &format, &access, &w, &h)==0 && access==SDL_TEXTUREACCESS_STREAMING){
        int i;
        Uint8 r,g,b,a,gray;
        if(!tex->locked)
            SDL_LockTexture(tex->tex,NULL,&tex->pixels,&tex->pitch);
        if (!tex->format)
            tex->format = SDL_AllocFormat(format);
        //SDL_ConvertPixels
        switch (tex->format->BitsPerPixel) {
            case 16:{
                Uint16 *pixels16 = (Uint16 *)tex->pixels;
                for (i=0;i<w*h;i++)
                {
                    SDL_GetRGBA(pixels16[i],tex->format,&r,&g,&b,&a);
                    gray = (r + (g<<1) + b) >> 2;
                    pixels16[i] = SDL_MapRGBA(tex->format,gray,gray,gray,a);
                }
                    }
            case 32:{
                Uint32 *pixels32 = (Uint32 *)tex->pixels;
                for (i=0;i<w*h;i++)
                {
                    SDL_GetRGBA(pixels32[i],tex->format,&r,&g,&b,&a);
                    gray = (r + (g<<1) + b) >> 2;
                    pixels32[i] = SDL_MapRGBA(tex->format,gray,gray,gray,a);
                }
            }
        }
        if (tex->format)
            SDL_FreeFormat(tex->format);
        tex->format = NULL;
        SDL_UnlockTexture(tex->tex);
        lua_pushboolean(L,1);
    }else
        lua_pushboolean(L,0);
    
    return 1;
}
//Surface灰度
int GGE_SurfaceToGrayscale(lua_State *L)
{
    SDL_Surface * sf = *(SDL_Surface**)luaL_checkudata(L, 1, "SDL_Surface");
    Uint8 r,g,b,a,gray;
    int i;

    switch (sf->format->BitsPerPixel) {
        case 16:{
            Uint16 *pixels16 = sf->pixels;
            for (i=0;i<sf->w*sf->h;i++)
            {
                SDL_GetRGBA(pixels16[i],sf->format,&r,&g,&b,&a);
                gray = (r + (g<<1) + b) >> 2;
                pixels16[i] = SDL_MapRGBA(sf->format,gray,gray,gray,a);
            }
                }
        case 32:{
            Uint32 *pixels32 = sf->pixels;
            for (i=0;i<sf->w*sf->h;i++)
            {
                SDL_GetRGBA(pixels32[i],sf->format,&r,&g,&b,&a);
                gray = (r + (g<<1) + b) >> 2;
                pixels32[i] = SDL_MapRGBA(sf->format,gray,gray,gray,a);
            }
        }
    }

    return 0;
}

int GGE_GetSurfacePixel(lua_State *L)
{
    SDL_Surface * sf = *(SDL_Surface**)luaL_checkudata(L, 1, "SDL_Surface");
    int x = (int)luaL_checkinteger(L,2);
    int y = (int)luaL_checkinteger(L,3);
    
    if ( x>=0 && y>=0 && x<sf->w && y<sf->h){
        Uint8 r,g,b,a;
        int bpp = sf->format->BytesPerPixel;
        Uint8 *p = (Uint8 *)sf->pixels + y * sf->pitch + x * bpp;
        switch (bpp) {
        case 2:{
            SDL_GetRGBA(*(Uint16 *)p,sf->format,&r,&g,&b,&a);
            lua_pushinteger(L,a);
            lua_pushinteger(L,r);
            lua_pushinteger(L,g);
            lua_pushinteger(L,b);
            }
        case 3:{}
        case 4:{
            SDL_GetRGBA(*(Uint32 *)p,sf->format,&r,&g,&b,&a);
            lua_pushinteger(L,a);
            lua_pushinteger(L,r);
            lua_pushinteger(L,g);
            lua_pushinteger(L,b);
            }
        }
    }else{
        lua_pushinteger(L,0);
        lua_pushinteger(L,0);
        lua_pushinteger(L,0);
        lua_pushinteger(L,0);
    }
    return 4;
}

int GGE_SetSurfacePixel(lua_State *L)
{
    SDL_Surface * sf = *(SDL_Surface**)luaL_checkudata(L, 1, "SDL_Surface");
    int x = (int)luaL_checkinteger(L,2);
    int y = (int)luaL_checkinteger(L,3);
    int r = (int)luaL_optinteger(L,4,0);
    int g = (int)luaL_optinteger(L,5,0);
    int b = (int)luaL_optinteger(L,6,0);
    int a = (int)luaL_optinteger(L,7,0);
    if ( x>=0 && y>=0 && x<sf->w && y<sf->h){
        int bpp = sf->format->BytesPerPixel;
        Uint8 *p = (Uint8 *)sf->pixels + y * sf->pitch + x * bpp;
        switch (bpp) {
        case 2:
            *(Uint16 *)p = SDL_MapRGBA(sf->format,r,g,b,a);
        case 3:{}
        case 4:
            *(Uint32 *)p = SDL_MapRGBA(sf->format,r,g,b,a);
        }
    }
    return 0;
}
//Surface取区域
int GGE_GetSurfaceRect(lua_State *L)
{
    SDL_Surface * sf = *(SDL_Surface**)luaL_checkudata(L, 1, "SDL_Surface");
    SDL_Rect rect;
    rect.x = (int)luaL_checkinteger(L,2);
    rect.y = (int)luaL_checkinteger(L,3);
    rect.w = (int)luaL_checkinteger(L,4);
    rect.h = (int)luaL_checkinteger(L,5);

    if (rect.x+rect.w<=sf->w && rect.y+rect.h<=sf->h)
    {
        SDL_Surface **ud;
        SDL_Surface * nsf = SDL_CreateRGBSurfaceWithFormat(SDL_SWSURFACE,rect.w,rect.h,sf->format->BitsPerPixel,sf->format->format);
        SDL_SetSurfaceBlendMode(sf,SDL_BLENDMODE_NONE);
        SDL_UpperBlit(sf,&rect,nsf,NULL);
        ud = (SDL_Surface**)lua_newuserdata(L, sizeof (SDL_Surface*));
        *ud = nsf;
        luaL_setmetatable(L, "SDL_Surface");
        return 1;
    }
    return 0;
}
//聊天窗口
int GGE_SetParent(lua_State *L)
{
    SDL_Window * Child = *(SDL_Window**)luaL_checkudata(L, 1, "SDL_Window");
    SDL_Window * Parent = *(SDL_Window**)luaL_checkudata(L, 2, "SDL_Window");
    SDL_SysWMinfo cinfo;
    SDL_SysWMinfo pinfo;
    LONG style,r;

    SDL_VERSION(&cinfo.version);
    SDL_VERSION(&pinfo.version);
    SDL_GetWindowWMInfo(Child, &cinfo);
    SDL_GetWindowWMInfo(Parent, &pinfo);
    
    r = SetWindowLongPtr(cinfo.info.win.window,GWLP_HWNDPARENT,(LONG_PTR)pinfo.info.win.window);//64位

    style = GetWindowLong(cinfo.info.win.window,GWL_STYLE);
    style &= ~(WS_MAXIMIZEBOX);//最大化
    style &= ~(WS_MINIMIZEBOX);//最小化
    style &= ~(WS_SYSMENU);//菜单
    SetWindowLong(cinfo.info.win.window,GWL_STYLE,style);
    
    return 0;
}

static const luaL_Reg window_funcs[] = {
    {"SetParent",GGE_SetParent},
    { NULL, NULL}
};

static const luaL_Reg texture_funcs[] = {
    {"GetTexturePixel"       , GGE_GetTexturePixel}       ,
    {"TextureToGrayscale"       , GGE_TextureToGrayscale}       ,
    { NULL, NULL}
};

static const luaL_Reg surface_funcs[] = {
    {"SurfaceToGrayscale"      , GGE_SurfaceToGrayscale}      ,
    {"GetSurfacePixel"      , GGE_GetSurfacePixel}      ,
    {"SetSurfacePixel"      , GGE_SetSurfacePixel}      ,
    {"GetSurfaceRect"      , GGE_GetSurfaceRect}      ,
    { NULL, NULL}
};

static const luaL_Reg sdl_funcs[] = {

    { NULL, NULL}
};

int bind_gge(lua_State* L)
{
    luaL_getmetatable(L, "SDL_Window");
    luaL_setfuncs(L, window_funcs, 0);
    lua_pop(L, 1);

    luaL_getmetatable(L, "SDL_Surface");
    luaL_setfuncs(L, surface_funcs, 0);
    lua_pop(L, 1);

    luaL_getmetatable(L, "SDL_Texture");
    luaL_setfuncs(L, texture_funcs, 0);
    lua_pop(L, 1);

    luaL_setfuncs(L, sdl_funcs, 0);
    return 0;
}