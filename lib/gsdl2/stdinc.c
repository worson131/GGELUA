#include "gge.h"

int LUA_malloc(lua_State *L)
{
    size_t size = luaL_checkinteger(L,1);
    void** ud = (void**)lua_newuserdata(L,sizeof(void*));
    *ud = SDL_malloc(size);
    luaL_setmetatable(L, "SDL_malloc");
    return 1;
}
int LUA_calloc(lua_State *L)
{
    size_t nmemb = luaL_checkinteger(L,1);
    size_t size = luaL_checkinteger(L,2);
    lua_pushlightuserdata(L,SDL_calloc(nmemb,size));
    return 1;
}
//SDL_realloc
int LUA_free(lua_State *L)
{
    void** ud = (void**)luaL_checkudata(L, 1, "SDL_malloc");

    SDL_free(*ud);
    return 0;
}

int bind_stdinc(lua_State* L)
{

    return 0;
}