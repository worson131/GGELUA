#include "gge.h"
#include "SDL_timer.h"

static int LUA_GetTicks(lua_State* L)
{
    lua_pushnumber(L, SDL_GetTicks());
    return 1;
}

static int LUA_GetPerformanceCounter(lua_State* L)
{
    lua_pushnumber(L, (lua_Number)SDL_GetPerformanceCounter());
    return 1;
}

static int LUA_GetPerformanceFrequency(lua_State* L)
{
    lua_pushnumber(L, (lua_Number)SDL_GetPerformanceFrequency());
    return 1;
}

static int LUA_Delay(lua_State* L)
{
    SDL_Delay((int)lua_tointeger(L, 1));
    return 0;
}

typedef struct _TimeInfo
{
    lua_State* L;
    int cb;
    SDL_TimerID id;
}TimeInfo;

static Uint32 LUA_TimerCallback(Uint32 interval, void* param) {
    TimeInfo* info = (TimeInfo*)param;

    return 0;
}

static int LUA_AddTimer(lua_State* L)
{
    //TimeInfo *info;
    //SDL_TimerID id;
    //info.L = L;
    //info.cb = luaL_ref(L,LUA_REGISTRYINDEX);
    //id = SDL_AddTimer((int)lua_tointeger(L, 1),SDL_TimerCallback,&info);
    //lua_pushinteger(L,id);
    return 1;
}

//SDL_RemoveTimer

static const luaL_Reg sdl_funcs[] = {
    //{"AddTimer"              , LUA_AddTimer}                ,
    {"Delay"                   , LUA_Delay}                   ,
    {"GetPerformanceCounter"   , LUA_GetPerformanceCounter}   ,
    {"GetPerformanceFrequency" , LUA_GetPerformanceFrequency} ,
    {"GetTicks"                , LUA_GetTicks}                ,
    //{"RemoveTimer"           , LUA_RemoveTimer}             ,
    { NULL, NULL}
};

int bind_timer(lua_State* L)
{
    luaL_setfuncs(L, sdl_funcs, 0);
    return 0;
}