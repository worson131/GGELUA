#include "gge.h"
#include "SDL_error.h"

static int LUA_SetError(lua_State* L)
{
    const char* err = luaL_checkstring(L, 1);

    lua_pushinteger(L, SDL_SetError("%s", err));
    return 1;
}

static int LUA_GetError(lua_State* L)
{
    const char* err = SDL_GetError();
    lua_pushstring(L, err);
    return 1;
}

static int LUA_ClearError(lua_State* L)
{
    SDL_ClearError();

    return 0;
}

static const luaL_Reg sdl_funcs[] = {
    {"ClearError" , LUA_ClearError} ,
    {"GetError"   , LUA_GetError}   ,
    {"SetError"   , LUA_SetError}   ,
    { NULL, NULL}
};

int bind_error(lua_State* L)
{
    luaL_setfuncs(L, sdl_funcs, 0);
    return 0;
}