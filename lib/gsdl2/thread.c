#include "gge.h"
#include "SDL_thread.h"

typedef struct
{
    lua_State *L;//主状态机
    lua_State *tL;//线程状态机
    lua_State *cL;//列表协程
    int ref_cL;//保存一下，防止释放
    SDL_Thread * th;
    int stop;
    int ms;
    int ref_list;//消息列表
    
}GGE_Thread;

static int ThreadFunction(void *data){
    GGE_Thread* ud = (GGE_Thread*)data;
    lua_State *L = ud->L;
    lua_State *tL = ud->tL;
    lua_State *cL = ud->cL;

    for (;;){
        lua_rawgeti(cL, LUA_REGISTRYINDEX, ud->ref_list);
        lua_pushnil(cL);
        while(lua_next(cL,-2)){//遍历list
            if (lua_type(cL,-1)==LUA_TTABLE)//value
            {
                lua_State* co = NULL;
                int n;
                const char* name;
                if (lua_type(cL,-2)==LUA_TTHREAD){//key=co
                    co = lua_tothread(cL,-2);
                }
                if (lua_geti(cL,-1,1)==LUA_TSTRING)//函数名称
                {
                    name = lua_tostring(cL,-1);
                    lua_pop(cL, 1);
                    if (lua_getglobal(tL,name)==LUA_TFUNCTION)
                    {
                        int narg = luaL_len(cL,-1);
                        int idx = lua_gettop(cL);
                        for (n=2;n<=narg;n++)//参数入栈
                            lua_geti(cL,idx,n);
                        
                        lua_xmove(cL, tL, narg-1);//移动到线程状态机
                        //TODO userdata meta
                        if (lua_pcall(tL,narg-1,co?-1:0,0)!=LUA_OK){
                            printf("%s\n",lua_tostring(tL,-1));
                            lua_settop(tL,0);
                        }else if(co){
                            int status, nres;
                            narg = lua_gettop(tL);
                            //for (n=1;n<=len;n++)
                            //{
                            //  switch(lua_type(tL,n)){
                            //  case LUA_TBOOLEAN:
                            //      lua_pushboolean(co,lua_toboolean(tL,n));break;
                            //  case LUA_TNUMBER:
                            //      lua_pushnumber(co,lua_tonumber(tL,n));break;
                            //  case LUA_TSTRING:
                            //      lua_pushstring(co,lua_tostring(tL,n));break;
                            //  default: 
                            //      lua_pushnil(co);break;
                            //  }
                            //}
                            lua_xmove(tL, co, narg);//返回值
                            //TODO userdata meta
                            status=lua_resume(co,L,narg,&nres);
                        }
                        //if (luaL_getmetafield(L, arg, "__name") == LUA_TSTRING)
                    }else
                        lua_pop(tL, 1);//no global function
                }else
                    lua_pop(cL, 1);//value[1] not a string
            }
            lua_pop(cL, 1);//remove value
            lua_pushvalue(cL,-1);//key
            lua_pushnil(cL);
            lua_settable(cL,-4);//t[co] = nil
        }
        lua_pop(cL, 1);//list

        SDL_Delay(ud->ms);
        if (ud->stop)
            break;
    }
    return 0;
}

static int  LUA_CreateThread(lua_State *L)
{
    size_t size;
    const char *code = luaL_checklstring(L,1,&size);
    const char *name = luaL_checkstring(L,2);
    int ms           = luaL_checkinteger(L,3);
    int list;
    lua_State *tL;

    lua_CFunction fn;
    const char* path,*cpath;
    luaL_checktype(L,4,LUA_TTABLE);
    list = luaL_ref(L,LUA_REGISTRYINDEX);//参数4 table
    //获取主状态机的配置
    lua_getglobal(L,LUA_LOADLIBNAME);
    lua_getfield(L, -1, "searchers");

    lua_rawgeti(L,-1,2);
    fn = lua_tocfunction(L,-1);
    lua_pop(L,2);//2,searchers
    lua_getfield(L, -1, "path");
    path = lua_tostring(L,-1);
    lua_pop(L,1);//path
    lua_getfield(L, -1, "cpath");
    cpath = lua_tostring(L,-1);
    lua_pop(L,2);//LUA_LOADLIBNAME,cpath

    tL = luaL_newstate();
    luaL_openlibs(tL);

    //printf("%d\n",lua_gettop(L));
    lua_getglobal(tL,LUA_LOADLIBNAME);
    lua_getfield(tL, -1, "searchers");
    lua_pushvalue(tL,-2);//把package设置为uv
    lua_pushcclosure(tL,fn,1);
    lua_seti(tL, -2, 2);//替换原搜索
    lua_pop(tL,1);//searchers
    lua_pushstring(tL,path);
    lua_setfield(tL,-2,"path");
    lua_pushstring(tL,cpath);
    lua_setfield(tL,-2,"cpath");
    lua_pop(tL,1);//LUA_LOADLIBNAME
    //printf("%d\n",lua_gettop(tL));
    if (luaL_loadbuffer(tL,code,size,name) || lua_pcall(tL,0,0,0)){
        lua_pushstring(L,lua_tostring(tL,-1));
        lua_close(tL);
        return 1;
    }
    

    {
        GGE_Thread*ud = (GGE_Thread*)lua_newuserdata(L,sizeof(GGE_Thread));
        luaL_setmetatable(L, "SDL_Thread");

        ud->stop = SDL_FALSE;
        ud->ms = ms;
        ud->tL = tL;
        ud->L = L;
        ud->cL = lua_newthread(L);
        ud->ref_cL = luaL_ref(L,LUA_REGISTRYINDEX);
        ud->ref_list = list;
        ud->th = SDL_CreateThread(ThreadFunction,name,ud);
    }

    return 1;
}
//SDL_CreateThreadWithStackSize
static int  LUA_GetThreadName(lua_State *L)
{
    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");
    lua_pushstring(L,SDL_GetThreadName(ud->th));
    return 1;
}

static int  LUA_GetThreadID(lua_State *L)
{
    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");
    lua_pushinteger(L,SDL_GetThreadID(ud->th));
    return 1;
}

static int  LUA_SetThreadPriority(lua_State *L)
{
    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");
    static const char *const opts[] = {"SDL_THREAD_PRIORITY_LOW", "SDL_THREAD_PRIORITY_NORMAL", "SDL_THREAD_PRIORITY_HIGH","SDL_THREAD_PRIORITY_TIME_CRITICAL", NULL};
    
    lua_pushinteger(L,SDL_SetThreadPriority((SDL_ThreadPriority)luaL_checkoption(L, 2, NULL, opts)));
    return 1;
}

static int  LUA_WaitThread(lua_State *L)
{
    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");
    int status;
    ud->stop = SDL_TRUE;
    
    SDL_WaitThread(ud->th,&status);
    luaL_unref(L,LUA_REGISTRYINDEX,ud->ref_cL);
    luaL_unref(L,LUA_REGISTRYINDEX,ud->ref_list);
    lua_close(ud->tL);
    return 0;
}

static int  LUA_DetachThread(lua_State *L)
{
    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");
    SDL_DetachThread(ud->th);
    ud->th = NULL;
    return 1;
}
//SDL_TLSCreate
//SDL_TLSGet
//SDL_TLSSet
static const luaL_Reg SDL_Thread_Meta[] = {
    {"__gc"    , LUA_WaitThread}    ,
    {"GetThreadName"     , LUA_GetThreadName}     ,
    {"GetThreadID"    , LUA_GetThreadID}    ,
    {"SetThreadPriority"     , LUA_SetThreadPriority}     ,

    {"DetachThread"    , LUA_DetachThread}    ,
    { NULL, NULL}
};

static const luaL_Reg sdl_funcs[] = {
    {"CreateThread"          , LUA_CreateThread}   ,    
    { NULL, NULL}
};

int bind_thread(lua_State *L)
{
    luaL_newmetatable(L,"SDL_Thread");
    luaL_setfuncs(L,SDL_Thread_Meta,0);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    lua_pop(L, 1);

    luaL_setfuncs(L,sdl_funcs,0);
    return 0;
}