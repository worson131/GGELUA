#include "gge.h"
#include "SDL_gesture.h"

static int LUA_RecordGesture(lua_State* L)
{
    return 0;
}

static int LUA_SaveAllDollarTemplates(lua_State* L)
{
    return 0;
}

static int LUA_SaveDollarTemplate(lua_State* L)
{
    return 0;
}

static int LUA_LoadDollarTemplates(lua_State* L)
{
    return 0;
}

static const luaL_Reg sdl_funcs[] = {

    { NULL, NULL}
};

int bind_gesture(lua_State* L)
{
    luaL_setfuncs(L, sdl_funcs, 0);
    return 0;
}