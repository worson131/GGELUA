#include "gge.h"
#include <SDL_mixer.h>

static int  LUA_Mix_Init(lua_State *L)
{
    int flags = (int)luaL_optinteger(L, 1,MIX_INIT_FLAC|MIX_INIT_MP3|MIX_INIT_OGG);
    lua_pushboolean(L,Mix_Init(flags)!=0);
    return 1;
}

static int  LUA_Mix_Quit(lua_State *L)
{
    Mix_Quit();
    return 0;
}

static int  LUA_Mix_OpenAudio(lua_State *L)
{
    int frequency = (int)luaL_checkinteger(L, 1);
    int format = (int)luaL_checkinteger(L, 2);
    int channels = (int)luaL_checkinteger(L, 3);
    int chunksize = (int)luaL_checkinteger(L, 4);
    lua_pushboolean(L,Mix_OpenAudio(frequency,format,channels,chunksize)==0);
    return 1;
}

static int  LUA_Mix_OpenAudioDevice(lua_State *L)
{
    int frequency = (int)luaL_checkinteger(L, 1);
    int format = (int)luaL_checkinteger(L, 2);
    int channels = (int)luaL_checkinteger(L, 3);
    int chunksize = (int)luaL_checkinteger(L, 4);
    const char* device = luaL_checkstring(L,5);
    int allowed_changes = (int)luaL_checkinteger(L, 6);

    lua_pushboolean(L,Mix_OpenAudioDevice(frequency,format,channels,chunksize,device,allowed_changes)==0);
    return 1;
}

static int  LUA_Mix_AllocateChannels(lua_State *L)
{
    int numchans = (int)luaL_checkinteger(L, 1);

    lua_pushinteger(L,Mix_AllocateChannels(numchans));
    return 1;
}

static int  LUA_Mix_QuerySpec(lua_State *L)
{
    int frequency,channels;
    Uint16 format;
    Mix_QuerySpec(&frequency,&format,&channels);
    lua_pushinteger(L,frequency);
    lua_pushinteger(L,format);
    lua_pushinteger(L,channels);
    return 3;
}

static int  LUA_Mix_LoadWAV(lua_State *L)
{
    const char* file = luaL_checkstring(L, 1);
    Mix_Chunk *mc = Mix_LoadWAV(file);
    Mix_Chunk **ud;
    if (mc){
        ud = (Mix_Chunk**)lua_newuserdata(L, sizeof (Mix_Chunk*));
        *ud = mc;
        luaL_setmetatable(L, "Mix_Chunk");
        return 1;
    }
    return 0;
}

static int  LUA_Mix_LoadMUS(lua_State *L)
{
    const char* file = luaL_checkstring(L, 1);
    Mix_Music *mm = Mix_LoadMUS(file);
    Mix_Music **ud;
    if (mm){
        ud = (Mix_Music**)lua_newuserdata(L, sizeof (Mix_Music*));
        *ud = mm;
        luaL_setmetatable(L, "Mix_Music");
        return 1;
    }
    return 0;
}
//Mix_LoadMUS_RW
//Mix_LoadMUSType_RW
//Mix_QuickLoad_WAV
//Mix_QuickLoad_RAW

static int  LUA_Mix_FreeChunk(lua_State *L)
{
    Mix_Chunk** mc = (Mix_Chunk**)luaL_checkudata(L, 1, "Mix_Chunk");
    if (*mc)
    {
        Mix_FreeChunk(*mc);
        *mc = NULL;
    }
    return 0;
}

static int  LUA_Mix_FreeMusic(lua_State *L)
{
    Mix_Music** mm = (Mix_Music**)luaL_checkudata(L, 1, "Mix_Music");
    if (*mm)
    {
        Mix_FreeMusic(*mm);
        *mm = NULL;
    }
    
    return 0;
}

static int  LUA_Mix_GetNumChunkDecoders(lua_State *L)
{
    lua_pushinteger(L,Mix_GetNumChunkDecoders());
    return 1;
}

static int  LUA_Mix_GetChunkDecoder(lua_State *L)
{
    int index = (int)luaL_checkinteger(L, 1);
    lua_pushstring(L,Mix_GetChunkDecoder(index));
    return 1;
}

static int  LUA_Mix_HasChunkDecoder(lua_State *L)
{
    const char* name = luaL_checkstring(L, 1);
    lua_pushboolean(L,Mix_HasChunkDecoder(name));
    return 1;
}

static int  LUA_Mix_GetNumMusicDecoders(lua_State *L)
{
    lua_pushinteger(L,Mix_GetNumMusicDecoders());
    return 1;
}

static int  LUA_Mix_GetMusicDecoder(lua_State *L)
{
    int index = (int)luaL_checkinteger(L, 1);
    lua_pushstring(L,Mix_GetMusicDecoder(index));
    return 1;
}

//static int    LUA_Mix_HasMusicDecoder(lua_State *L)
//{
//  const char* name = luaL_checkstring(L, 1);
//  lua_pushboolean(L,Mix_HasMusicDecoder(name));
//  return 1;
//}

static int  LUA_Mix_GetMusicType(lua_State *L)
{
    Mix_Music* mm = *(Mix_Music**)luaL_checkudata(L, 1, "Mix_Music");
    lua_pushinteger(L,Mix_GetMusicType(mm));
    return 1;
}

//Mix_SetPostMix
//Mix_HookMusic
//Mix_HookMusicFinished
//Mix_GetMusicHookData
//Mix_ChannelFinished
//Mix_RegisterEffect
//Mix_UnregisterEffect
static int  LUA_Mix_UnregisterAllEffects(lua_State *L)
{
    int channel = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_UnregisterAllEffects(channel));
    return 1;
}

static int  LUA_Mix_SetPanning(lua_State *L)//左右？
{
    int channel = (int)luaL_checkinteger(L, 1);
    Uint8 left = (Uint8)luaL_checkinteger(L, 2);
    Uint8 right = (Uint8)luaL_checkinteger(L, 3);
    lua_pushinteger(L,Mix_SetPanning(channel,left,right));
    return 1;
}

static int  LUA_Mix_SetPosition(lua_State *L)//3D？
{
    int channel = (int)luaL_checkinteger(L, 1);
    Sint16 angle = (Sint16)luaL_checkinteger(L, 2);
    Uint8 distance = (Uint8)luaL_checkinteger(L, 3);
    lua_pushinteger(L,Mix_SetPosition(channel,angle,distance));
    return 1;
}

static int  LUA_Mix_SetDistance(lua_State *L)
{
    int channel = (int)luaL_checkinteger(L, 1);
    Uint8 distance = (Uint8)luaL_checkinteger(L, 2);
    lua_pushinteger(L,Mix_SetDistance(channel,distance));
    return 1;
}

static int  LUA_Mix_SetReverseStereo(lua_State *L)
{
    int channel = (int)luaL_checkinteger(L, 1);
    int flip = (int)luaL_checkinteger(L, 2);
    lua_pushinteger(L,Mix_SetReverseStereo(channel,flip));
    return 1;
}
static int  LUA_Mix_ReserveChannels(lua_State *L)//固定通道
{
    int num = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_ReserveChannels(num));
    return 1;
}

static int  LUA_Mix_GroupChannel(lua_State *L)//通道分组
{
    int which = (int)luaL_checkinteger(L, 1);
    int tag = (int)luaL_checkinteger(L, 2);
    lua_pushinteger(L,Mix_GroupChannel(which,tag));
    return 1;
}

static int  LUA_Mix_GroupChannels(lua_State *L)//通道分组
{
    int from = (int)luaL_checkinteger(L, 1);
    int to = (int)luaL_checkinteger(L, 2);
    int tag = (int)luaL_checkinteger(L, 2);
    lua_pushinteger(L,Mix_GroupChannels(from,to,tag));
    return 1;
}

static int  LUA_Mix_GroupAvailable(lua_State *L)//查找分组可用通道
{
    int tag = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_GroupAvailable(tag));
    return 1;
}

static int  LUA_Mix_GroupCount(lua_State *L)//分组数量
{
    int tag = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_GroupCount(tag));
    return 1;
}

static int  LUA_Mix_GroupOldest(lua_State *L)//查找分组中最早播放的
{
    int tag = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_GroupOldest(tag));
    return 1;
}

static int  LUA_Mix_GroupNewer(lua_State *L)//查找分组中最后播放的
{
    int tag = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_GroupNewer(tag));
    return 1;
}

static int  LUA_Mix_PlayChannel(lua_State *L)
{
    Mix_Chunk* mc = *(Mix_Chunk**)luaL_checkudata(L, 1, "Mix_Chunk");
    int channel = (int)luaL_checkinteger(L, 2);
    int loops = (int)luaL_checkinteger(L, 3);
    lua_pushinteger(L,Mix_PlayChannel(channel,mc,loops));
    return 1;
}

static int  LUA_Mix_PlayChannelTimed(lua_State *L)
{
    Mix_Chunk* mc = *(Mix_Chunk**)luaL_checkudata(L, 1, "Mix_Chunk");
    int channel = (int)luaL_checkinteger(L, 2);
    int loops = (int)luaL_checkinteger(L, 3);
    int ticks = (int)luaL_checkinteger(L, 4);
    lua_pushinteger(L,Mix_PlayChannelTimed(channel,mc,loops,ticks));
    return 1;
}

static int  LUA_Mix_PlayMusic(lua_State *L)
{
    Mix_Music* mm = *(Mix_Music**)luaL_checkudata(L, 1, "Mix_Music");
    int loops = (int)luaL_checkinteger(L, 2);
    lua_pushinteger(L,Mix_PlayMusic(mm,loops));
    return 1;
}

static int  LUA_Mix_FadeInMusic(lua_State *L)
{
    Mix_Music* mm = *(Mix_Music**)luaL_checkudata(L, 1, "Mix_Music");
    int loops = (int)luaL_checkinteger(L, 2);
    int ms = (int)luaL_checkinteger(L, 3);
    lua_pushinteger(L,Mix_FadeInMusic(mm,loops,ms));
    return 1;
}

static int  LUA_Mix_FadeInMusicPos(lua_State *L)
{
    Mix_Music* mm = *(Mix_Music**)luaL_checkudata(L, 1, "Mix_Music");
    int loops = (int)luaL_checkinteger(L, 2);
    int ms = (int)luaL_checkinteger(L, 3);
    double position = luaL_checknumber(L, 4);
    lua_pushinteger(L,Mix_FadeInMusicPos(mm,loops,ms,position));
    return 1;
}

static int  LUA_Mix_FadeInChannel(lua_State *L)//淡入淡出
{
    Mix_Chunk* mc = *(Mix_Chunk**)luaL_checkudata(L, 1, "Mix_Chunk");
    int channel = (int)luaL_checkinteger(L, 2);
    int loops = (int)luaL_checkinteger(L, 3);
    int ms = (int)luaL_checkinteger(L, 4);
    lua_pushinteger(L,Mix_FadeInChannel(channel,mc,loops,ms));
    return 1;
}

static int  LUA_Mix_FadeInChannelTimed(lua_State *L)
{
    Mix_Chunk* mc = *(Mix_Chunk**)luaL_checkudata(L, 1, "Mix_Chunk");
    int channel = (int)luaL_checkinteger(L, 2);
    int loops = (int)luaL_checkinteger(L, 3);
    int ms = (int)luaL_checkinteger(L, 4);
    int ticks = (int)luaL_checkinteger(L, 5);
    lua_pushinteger(L,Mix_FadeInChannelTimed(channel,mc,loops,ms,ticks));
    return 1;
}

static int  LUA_Mix_Volume(lua_State *L)
{
    int channel = (int)luaL_checkinteger(L, 1);
    int volume = (int)luaL_checkinteger(L, 2);
    lua_pushinteger(L,Mix_Volume(channel,volume));
    return 1;
}

static int  LUA_Mix_VolumeChunk(lua_State *L)
{
    Mix_Chunk* mc = *(Mix_Chunk**)luaL_checkudata(L, 1, "Mix_Chunk");
    int volume = (int)luaL_checkinteger(L, 2);

    lua_pushinteger(L,Mix_VolumeChunk(mc,volume));
    return 1;
}

static int  LUA_Mix_VolumeMusic(lua_State *L)
{
    int volume = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_VolumeMusic(volume));
    return 1;
}

static int  LUA_Mix_HaltChannel(lua_State *L)
{
    int channel = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_HaltChannel(channel));
    return 1;
}

static int  LUA_Mix_HaltGroup(lua_State *L)
{
    int tag = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_HaltGroup(tag));
    return 1;
}

static int  LUA_Mix_HaltMusic(lua_State *L)
{
    lua_pushinteger(L,Mix_HaltMusic());
    return 1;
}

static int  LUA_Mix_ExpireChannel(lua_State *L)//加时间
{
    int channel = (int)luaL_checkinteger(L, 1);
    int ticks = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_ExpireChannel(channel,ticks));
    return 1;
}

static int  LUA_Mix_FadingMusic(lua_State *L)
{
    lua_pushinteger(L,Mix_FadingMusic());
    return 1;
}

static int  LUA_Mix_FadingChannel(lua_State *L)
{
    int which = (int)luaL_checkinteger(L, 1);
    lua_pushinteger(L,Mix_FadingChannel(which));
    return 1;
}

static int  LUA_Mix_Pause(lua_State *L)
{
    int channel = (int)luaL_checkinteger(L, 1);
    Mix_Pause(channel);
    return 0;
}

static int  LUA_Mix_Resume(lua_State *L)
{
    int channel = (int)luaL_checkinteger(L, 1);
    Mix_Resume(channel);
    return 0;
}

static int  LUA_Mix_Paused(lua_State *L)
{
    int which = (int)luaL_checkinteger(L, 1);
    lua_pushboolean(L,Mix_Paused(which));
    return 1;
}

static int  LUA_Mix_PauseMusic(lua_State *L)
{
    Mix_PauseMusic();
    return 0;
}

static int  LUA_Mix_ResumeMusic(lua_State *L)
{
    Mix_ResumeMusic();
    return 0;
}

static int  LUA_Mix_RewindMusic(lua_State *L)
{
    Mix_RewindMusic();
    return 0;
}

static int  LUA_Mix_PausedMusic(lua_State *L)
{
    lua_pushboolean(L,Mix_PausedMusic());
    return 1;
}

static int  LUA_Mix_SetMusicPosition(lua_State *L)
{
    double position = (double)luaL_checknumber(L, 1);
    lua_pushboolean(L,Mix_SetMusicPosition(position)==0);
    return 1;
}

static int  LUA_Mix_Playing(lua_State *L)
{
    int channel = (int)luaL_checkinteger(L, 1);
    lua_pushboolean(L,Mix_Playing(channel));
    return 1;
}

static int  LUA_Mix_PlayingMusic(lua_State *L)
{
    lua_pushboolean(L,Mix_PlayingMusic());
    return 1;
}
//Mix_SetMusicCMD
//static int    LUA_Mix_SetSynchroValue(lua_State *L)
//{
//  int value = luaL_checkinteger(L, 1);
//  lua_pushinteger(L,Mix_SetSynchroValue(value));
//  return 1;
//}
//static int    LUA_Mix_GetSynchroValue(lua_State *L)
//{
//  lua_pushinteger(L,Mix_GetSynchroValue());
//  return 1;
//}
//Mix_SetSoundFonts
//Mix_GetSoundFonts
//Mix_EachSoundFont
//Mix_GetChunk
static int  LUA_Mix_CloseAudio(lua_State *L)
{
    Mix_CloseAudio();
    return 0;
}

static const luaL_Reg mix_funcs[] = {
    {"Init"         , LUA_Mix_Init}         ,
    {"Quit"         , LUA_Mix_Quit}         ,
    {"OpenAudio"         , LUA_Mix_OpenAudio}         ,
    {"CloseAudio"         , LUA_Mix_CloseAudio}         ,
    {"OpenAudioDevice"      , LUA_Mix_OpenAudioDevice}      ,
    {"AllocateChannels" , LUA_Mix_AllocateChannels} ,
    {"QuerySpec" , LUA_Mix_QuerySpec} ,
    {"LoadWAV" , LUA_Mix_LoadWAV} ,
    {"LoadMUS" , LUA_Mix_LoadMUS} ,
    {"GetNumChunkDecoders" , LUA_Mix_GetNumChunkDecoders} ,
    {"GetChunkDecoder" , LUA_Mix_GetChunkDecoder} ,
    {"HasChunkDecoder" , LUA_Mix_HasChunkDecoder} ,
    {"GetNumMusicDecoders" , LUA_Mix_GetNumMusicDecoders} ,
    {"GetMusicDecoder" , LUA_Mix_GetMusicDecoder} ,
    
    {"UnregisterAllEffects" , LUA_Mix_UnregisterAllEffects} ,
    {"SetPanning" , LUA_Mix_SetPanning} ,
    {"SetPosition" , LUA_Mix_SetPosition} ,
    {"SetDistance" , LUA_Mix_SetDistance} ,
    {"SetReverseStereo" , LUA_Mix_SetReverseStereo} ,
    {"ReserveChannels" , LUA_Mix_ReserveChannels} ,
    {"GroupChannel" , LUA_Mix_GroupChannel} ,
    {"GroupChannels" , LUA_Mix_GroupChannels} ,
    {"GroupAvailable" , LUA_Mix_GroupAvailable} ,
    {"GroupCount" , LUA_Mix_GroupCount} ,
    {"GroupOldest" , LUA_Mix_GroupOldest} ,
    {"GroupNewer" , LUA_Mix_GroupNewer} ,

    {"Volume" , LUA_Mix_Volume} ,
    {"VolumeMusic" , LUA_Mix_VolumeMusic} ,
    {"HaltChannel" , LUA_Mix_HaltChannel} ,
    {"HaltGroup" , LUA_Mix_HaltGroup} ,
    {"HaltMusic" , LUA_Mix_HaltMusic} ,
    {"ExpireChannel" , LUA_Mix_ExpireChannel} ,
    {"FadingMusic" , LUA_Mix_FadingMusic} ,
    {"FadingChannel" , LUA_Mix_FadingChannel} ,
    {"Pause" , LUA_Mix_Pause} ,
    {"Resume" , LUA_Mix_Resume} ,
    {"Paused" , LUA_Mix_Paused} ,
    {"PauseMusic" , LUA_Mix_PauseMusic} ,
    {"ResumeMusic" , LUA_Mix_ResumeMusic} ,
    {"RewindMusic" , LUA_Mix_RewindMusic} ,
    {"PausedMusic" , LUA_Mix_PausedMusic} ,
    {"SetMusicPosition" , LUA_Mix_SetMusicPosition} ,
    {"Playing" , LUA_Mix_Playing} ,
    {"PlayingMusic" , LUA_Mix_PlayingMusic} ,
    //{"SetSynchroValue" , LUA_Mix_SetSynchroValue} ,
    //{"GetSynchroValue" , LUA_Mix_GetSynchroValue} ,
    { NULL          , NULL          }
};

static const luaL_Reg chunk_funcs[] = {
    {"__gc"         , LUA_Mix_FreeChunk}         ,
    {"PlayChannel"         , LUA_Mix_PlayChannel}         ,
    {"PlayChannelTimed"         , LUA_Mix_PlayChannelTimed}         ,
    {"FadeInChannel"      , LUA_Mix_FadeInChannel}      ,
    {"FadeInChannelTimed" , LUA_Mix_FadeInChannelTimed} ,
    {"VolumeChunk" , LUA_Mix_VolumeChunk} ,
    { NULL          , NULL          }
};

static const luaL_Reg music_funcs[] = {
    {"__gc"         , LUA_Mix_FreeMusic}         ,
    {"GetMusicType" , LUA_Mix_GetMusicType} ,
    {"PlayMusic"         , LUA_Mix_PlayMusic}         ,
    {"FadeInMusic"         , LUA_Mix_FadeInMusic}         ,
    {"FadeInMusicPos"      , LUA_Mix_FadeInMusicPos}      ,
    //{"LoadTyped_RW" , LUA_IMG_LoadTyped_RW} ,
    { NULL          , NULL          }
};

LUALIB_API int luaopen_gsdl2_mixer(lua_State *L)
{
    luaL_newmetatable(L,"Mix_Chunk");
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L,chunk_funcs,0);
    lua_pop(L, 1);

    luaL_newmetatable(L,"Mix_Music");
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L,music_funcs,0);
    lua_pop(L, 1);

    luaL_newlib(L,mix_funcs);
    return 1;
}