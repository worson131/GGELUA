#include "help.h"
#include <SDL.h>
#include <d3d9.h>
#include "backends/imgui_impl_sdl.h"
#include "backends/imgui_impl_dx9.h"

static SDL_Renderer * g_Renderer = NULL;
int Widgets(lua_State* L);

int Event(lua_State* L){
	SDL_Event * ev = (SDL_Event*)luaL_checkudata(L, 1, "SDL_Event");
	lua_pushboolean(L,ImGui_ImplSDL2_ProcessEvent(ev));
	return 1;
}

int Render(lua_State* L){
	SDL_RenderFlush(g_Renderer);
	ImGui::Render();
	ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
	return 0;
}

int GetIO(lua_State* L){
	ImGuiIO& io = ImGui::GetIO();
	lua_pushboolean(L,io.WantCaptureMouse);
	return 1;
}

int GetStyle(lua_State* L){

	return 0;
}

int NewFrame(lua_State* L){
	ImGui_ImplDX9_NewFrame();
	ImGui_ImplSDL2_NewFrame();
	ImGui::NewFrame();
	return 0;
}

int EndFrame(lua_State* L){
	ImGui::EndFrame();
	return 0;
}

int ShowDemoWindow(lua_State* L){
	bool v;
	getbyref(L,1,v);
	ImGui::ShowDemoWindow(&v);
	setbyref(L,1,v);
	return 0;
}

int ShowAboutWindow(lua_State* L){
	bool v;
	getbyref(L,1,v);
	ImGui::ShowAboutWindow(&v);
	setbyref(L,1,v);
	return 0;
}

int ShowMetricsWindow(lua_State* L){
	bool v;
	getbyref(L,1,v);
	ImGui::ShowMetricsWindow(&v);
	setbyref(L,1,v);
	return 0;
}

int ShowStyleEditor(lua_State* L){
	ImGui::ShowStyleEditor();
	return 0;
}

int ShowStyleSelector(lua_State* L){
	bool r = ImGui::ShowStyleSelector(luaL_checkstring(L,1));
	lua_pushboolean(L,r);
	return 1;
}

int ShowFontSelector(lua_State* L){
	ImGui::ShowFontSelector(luaL_checkstring(L,1));
	return 0;
}

int ShowUserGuide(lua_State* L){
	ImGui::ShowUserGuide();
	return 0;
}

int GetVersion(lua_State* L){
	lua_pushstring(L,ImGui::GetVersion());
	return 1;
}

int StyleColorsDark(lua_State* L){
	ImGui::StyleColorsDark();
	return 0;
}

int StyleColorsClassic(lua_State* L){
	ImGui::StyleColorsClassic();
	return 0;
}

int StyleColorsLight(lua_State* L){
	ImGui::StyleColorsLight();
	return 0;
}

int Init(lua_State* L){
	SDL_Window * window = *(SDL_Window**)luaL_checkudata(L, 1, "SDL_Window");

	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();// (void)io;

	io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\simsun.ttc", 16.0f, NULL, io.Fonts->GetGlyphRangesChineseFull());

	ImGui_ImplSDL2_InitForD3D(window);
	g_Renderer = SDL_GetRenderer(window);
	IDirect3DDevice9 *device = SDL_RenderGetD3D9Device(g_Renderer);
	if (device){
		ImGui_ImplDX9_Init(device);
		device->Release();
	}
	lua_pushboolean(L,device!=NULL);
	return 1;
}

extern "C" LUALIB_API
	int luaopen_gimgui(lua_State* L)
{
	luaL_Reg funcs[] = {
		{"Init",Init},
		{"Event",Event},

		//CreateContext
		//DestroyContext
		//GetCurrentContext
		//SetCurrentContext
		{"GetIO",GetIO},
		{"GetStyle",GetStyle},
		{"NewFrame",NewFrame},
		{"EndFrame",EndFrame},
		{"Render", Render},

		{"ShowDemoWindow",ShowDemoWindow},
		{"ShowMetricsWindow",ShowMetricsWindow},
		{"ShowAboutWindow",ShowAboutWindow},
		{"ShowStyleEditor",ShowStyleEditor},
		{"ShowStyleSelector",ShowStyleSelector},
		{"ShowFontSelector",ShowFontSelector},
		{"ShowUserGuide",ShowUserGuide},
		{"GetVersion",GetVersion},

		{"StyleColorsDark",StyleColorsDark},
		{"StyleColorsClassic",StyleColorsClassic},
		{"StyleColorsLight",StyleColorsLight},

		{NULL, NULL},
	};
	//setvbuf(stdout, NULL, _IONBF, 0);
	luaL_newlib(L, funcs);
	Widgets(L);
	return 1;
}