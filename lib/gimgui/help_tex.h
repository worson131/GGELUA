#include <sdl/SDL.h>
#include <dx9/d3d9.h>

struct SDL_SW_YUVTexture
{
	Uint32 format;
	Uint32 target_format;
	int w, h;
	Uint8 *pixels;

	/* These are just so we don't have to allocate them separately */
	Uint16 pitches[3];
	Uint8 *planes[3];

	/* This is a temporary surface in case we have to stretch copy */
	SDL_Surface *stretch;
	SDL_Surface *display;
};
struct SDL_Texture
{
	const void *magic;
	Uint32 format;              /**< The pixel format of the texture */
	int access;                 /**< SDL_TextureAccess */
	int w;                      /**< The width of the texture */
	int h;                      /**< The height of the texture */
	int modMode;                /**< The texture modulation mode */
	SDL_BlendMode blendMode;    /**< The texture blend mode */
	SDL_ScaleMode scaleMode;    /**< The texture scale mode */
	Uint8 r, g, b, a;           /**< Texture modulation values */

	SDL_Renderer *renderer;

	/* Support for formats not supported directly by the renderer */
	SDL_Texture *native;
	SDL_SW_YUVTexture *yuv;
	void *pixels;
	int pitch;
	SDL_Rect locked_rect;
	SDL_Surface *locked_surface;  /**< Locked region exposed as a SDL surface */

	Uint32 last_command_generation; /* last command queue generation this texture was in. */

	void *driverdata;           /**< Driver specific texture representation */

	SDL_Texture *prev;
	SDL_Texture *next;
};

typedef struct
{
	SDL_bool dirty;
	int w, h;
	DWORD usage;
	Uint32 format;
	D3DFORMAT d3dfmt;
	IDirect3DTexture9 *texture;
	IDirect3DTexture9 *staging;
} D3D_TextureRep;

typedef struct
{
	D3D_TextureRep texture;
	D3DTEXTUREFILTERTYPE scaleMode;

	/* YV12 texture support */
	SDL_bool yuv;
	D3D_TextureRep utexture;
	D3D_TextureRep vtexture;
	Uint8 *pixels;
	int pitch;
	SDL_Rect locked_rect;
} D3D_TextureData;