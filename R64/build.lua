--[[
    @Author       : baidwwy
    @Date         : 2021-02-08 15:51:46
    @LastEditTime : 2021-03-10 10:00:01
--]]
io.stdout:setvbuf('no',0)
local gge = package.loadlib("ggelua", "luaopen_ggelua")()
local _ENV = setmetatable(gge, {__index=_G})

local tt = os.clock()

local runpath = getrunpath()
local curpath = getcurpath()--arg[1]
local luafolder = arg[2] or 'lua'
local exetype = arg[3] or 'ggelua.exe'
print(runpath,curpath,luafolder,exetype)
package.cpath = ("?.dll;lib/?.dll;!/?.dll;!/lib/?.dll"):gsub('!',runpath)
local lfs = require("lfs")
local cjson = require("cjson.safe")
local cmsgpack = require("cmsgpack.safe")

function 复制文件(old,new)
    local rf<close> = io.open(utf8toansi(old),"rb")
    if rf then
        local wf<close> = io.open(utf8toansi(new),"wb")
        if wf then
            wf:write(rf:read("a"))
            return true
        end
    end
    return false
end

function 读取文件(path)
    local file<close> = io.open(utf8toansi(path), 'rb');
    if file then
        return file:read('a')
    end
end

function 写出文件(path,data)
    local file<close> = io.open(utf8toansi(path),"wb")
    if file then
        file:write(data)
        return true
    end
    return false
end

function 遍历目录(path)
    path = utf8toansi(path)
    local dir,u = lfs.dir(path)
    local pt = {}
    return function ()
        repeat
            local file = dir(u)
            if file then
                local f = path..'/'..file
                local attr = lfs.attributes (f)
                if attr and attr.mode == "directory" then
                    if file ~= "." and file ~= ".." then
                        table.insert(pt, f)
                    end
                    file = "."
                else
                    return ansitoutf8(f)
                end
            elseif pt[1] then
                path = table.remove(pt, 1)
                dir,u = lfs.dir(path)
                file = "."
            end
        until file ~= "."
    end
end

function 处理路径(path)
    path = path:lower()--小写
    path = path:gsub('\\','/')
    return path
end

--local cfg = 读取文件(curpath..'/?.code-workspace')
-- cfg = cjson.decode(cfg)
-- if cfg and cfg.ggelua and cfg.ggelua.library then
--     for i,v in ipairs(cfg.ggelua.library) do
--         print('code-workspace',i,v)
--     end
-- end
--启动脚本
local data,core = {}
do
    local r = assert(读取文件(runpath.."/ggelua.lua"), '读取失败:ggelua.lua')
    local fun = assert(load(r,"ggelua.lua"))
    core = string.dump(fun)
end


print('-------------------------------------------------------------')
print("编译GGE脚本")
print('-------------------------------------------------------------')
for path in 遍历目录(runpath.."/lua") do
    path = 处理路径(path)
    if path:sub(-3)=='lua' then
        local r = assert(读取文件(path), '读取失败:'..path)
        path = path:sub(#runpath+2)--删除绝对路径
        local fun = assert(load(r,path))
        data[path] = string.dump(fun)
        print('- '..path)
    end
end


print('-------------------------------------------------------------')
print("编译项目脚本")
print('-------------------------------------------------------------')
for path in 遍历目录(luafolder) do
    path = 处理路径(path)
    if path:sub(-3)=='lua' then
        local r = assert(读取文件(path), '读取失败:'..path)
        local fun = assert(load(r,path))
        data[path] = string.dump(fun)
        print('- '..path)
    end
end
print('-------------------------------------------------------------')


data = cmsgpack.pack(data)

复制文件(runpath.."/lua54.dll","lua54.dll")
复制文件(runpath.."/ggelua.dll","ggelua.dll")

if not 复制文件(runpath.."/"..exetype,"ggelua.exe") then
    error('写出失败！')
end
--print(string.packsize("<I4I4I4"))
local file<close> = io.open("ggelua.exe","r+b")
if file then
    if file:seek('end',-12) then
        local glue = file:read(12)
        if glue and #glue==12 then
            local sig,s1,s2 = string.unpack("<I4I4I4",glue)
            if sig==0x20454747 then
                file:seek('end',-(12+s1+s2))
            end
            file:seek('cur')--没有这句，会有奇怪的数据
            file:write(core)
            file:write(data)
            file:write(string.pack("<I4I4I4",0x20454747,#core,#data))
        else
            goto 失败
        end
    else
        goto 失败
    end
else
    goto 失败
end

print('编译完成\n用时:'..os.clock()-tt..'秒')
do
    return 
end
::失败::
error('写出失败！')