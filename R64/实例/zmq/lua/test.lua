--[[
    @Author       : GGELUA
    @Date         : 2021-02-17 20:36:36
    @LastEditTime : 2021-02-18 09:23:22
--]]
local zmq = require "lzmq"

local context = zmq.init(1)
local server = context:socket(zmq.REP)
print(server:bind("tcp://127.0.0.1:5566"))

while true do
    local request = server:recv()
    print(request)
    server:send("World")
end
server:close()
context:term()