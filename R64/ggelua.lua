--[[
    @Author       : baidwwy
    @Date         : 2020-09-22 19:49:01
    @LastEditTime : 2021-03-16 17:58:25
--]]
io.stdout:setvbuf('no',0)
local gge = assert(package.loadlib("ggelua", "luaopen_ggelua"),'加载"ggelua.dll"失败')()

local entry,data,nstate = ...
gge.isdebug = data==nil
gge.entry = entry

local function 分割路径(path)
    local t,n = {},1
    for match in path:gmatch("([^;]+)") do
        t[n] = match
        n=n+1
    end
    return t
end
--lua脚本搜索
local lpath = "?.lua;lua/?.lua;lua/?/?.lua;!/lua/?.lua;!/lua/?/?.lua"
lpath = lpath:gsub('!',gge.getrunpath())
lpath_ = 分割路径(lpath)
--lua模块搜索
local cpath = "?.dll;lib/?.dll;!/?.dll;!/lib/?.dll"
if gge.getplatform()=='Android' then--安卓模块加 lib?
    cpath = "lib?.dll;lib/lib?.dll;!/lib?.dll;!/lib/lib?.dll"
end
cpath = cpath:gsub('!',gge.getrunpath())


package.path = nil
package.cpath = nil
setmetatable(package, {
    __newindex=function (t,k,v)
        if k=='path' then
            lpath = v:lower():gsub('\\','/')
            lpath_ = 分割路径(lpath)
        elseif k=='cpath' then
            cpath = v
        else
            rawset(t, k, v)
        end
    end,
    __index = function (t,k)
        if k=='path' then
            return lpath
        elseif k=='cpath' then
            return cpath
        end
    end
})

local m_unpack = require("cmsgpack.safe").unpack

if data then
    data = m_unpack(data)
end

local function 处理路径(path)
    --相对路径
    -- local t = {}
    -- for match in path:gmatch("([^/]+)") do
    --     if match=='..' then
    --         table.remove(t)
    --     elseif match~='.' then
    --         table.insert(t, match)
    --     end
    -- end
    -- path = table.concat(t, "/")

    path = path:lower()--小写
    path = path:gsub('%.','/')
    path = path:gsub('\\','/')
    return path
end

local 读取文件,是否存在
if gge.isdebug then
    function 是否存在(path)
        local file<close> = io.open(path,"rb")
        if file then
            return true
        end
    end

    function 读取文件(path)
        local file<close> = io.open(path,'rb');
        if file then
            return file:read('a')
        end
    end

    function package.dir(path,...)
        if select("#", ...)>0 then
            path = path:format(...)
        end
        local lfs = require("lfs")
        local dir,u = lfs.dir(path)
        local pt = {}
        return function ()
            repeat
                local file = dir(u)
                if file then
                    local f = path..'/'..file
                    local attr = lfs.attributes (f)
                    if attr and attr.mode == "directory" then
                        if file ~= "." and file ~= ".." then
                            table.insert(pt, f)
                        end
                        file = "."
                    else
                        return f
                    end
                elseif pt[1] then
                    path = table.remove(pt, 1)
                    dir,u = lfs.dir(path)
                    file = "."
                end
            until file ~= "."
        end
    end
else
    function 是否存在(file)
        return data[file]~=nil
    end

    function 读取文件(file)
        return data[file]
    end

    function package.dir(path,...)
        if select("#", ...)>0 then
            path = path:format(...)
        end
        local k,v
        return function ()
            repeat
                k,v = next(data, k)
                if k and k:find(path) then
                    return k,v
                end
            until not k
        end
    end
end

local function 搜索路径(path)
    for _,v in ipairs(lpath_) do
        local file = v:gsub('?',path)
        if 是否存在(file) then
            return file
        end
    end
end

table.insert(package.searchers, 1, function (path)
    path = 处理路径(path)
    path = 搜索路径(path)
    if path then
        local r,err = load(读取文件(path),path)
        return r or error(err,2)
    end
end)

function import(path,env,...)
    path = path:gsub('\\','/'):lower()
    local data = 读取文件(path)
    if data then
        return assert(load(data,path,"bt",env))(...)
    end
end

require(entry)
