--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-04 17:32:48
--]]

local string = string
local _ENV = require "ghpsocket.hpsocket"

function 取版本()
    local v = GetHPSocketVersion()
    return string.format("%d.%d.%d.%d", v>>24,v>>16&255,v>>8&255,v&255)
end

function 取主机地址(host)
    return GetIPAddress(host)
end

SS_STARTING  = "正在启动"
SS_STARTED   = "已经启动"
SS_STOPPING  = "正在停止"
SS_STOPPED   = "已经停止"
return _ENV