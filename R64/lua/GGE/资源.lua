--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-09 04:08:05
--]]

local _ENV = require("SDL")
local GGE资源包


local GGE资源 = class"GGE资源"

function GGE资源:初始化()
    self._path = {}
    self._pack = {}
end

function GGE资源:添加资源包(file,psd)
    self._pack[file] = GGE资源包(file,psd)
    return self._pack[file]
end

function GGE资源:删除资源包(file)
    self._pack[file] = nil
end

function GGE资源:添加路径(p)
    self._path[p] = true
end

function GGE资源:删除路径(p)
    self._path[p] = nil
end

function GGE资源:是否存在(path,...)
    if select("#", ...)>0 then
        path = path:format(...)
    end
    --绝对路径
    local file<close> = RWFromFile(path,"r")
    if file then
        return path
    end
    --搜索路径
    for k,_ in pairs(self._path) do
        local file<close> = RWFromFile(k.."/"..path,"r")
        if file then
            return k.."/"..path,k
        end
    end
    --搜索包
    
    for f,p in pairs(self._pack) do
        if p:是否存在(path) then
            return p,f
        end
    end
    return false
end

function GGE资源:读数据(path,...)
    if select("#", ...)>0 then
        path = path:format(...)
    end
    local r = self:是否存在(path)
    if r then
        if type(r)=='string' then
            return LoadFile(r)--SDL
        end
        return r:读数据(path)
    end
    return nil
end

function GGE资源:取纹理(...)
    local data = self:读数据(...)
    if data then
        return require("SDL.纹理")(data,#data)
    end
end

function GGE资源:取精灵(...)
    local tex = self:取纹理(...)
    if tex then
        return require("SDL.精灵")(tex)
    end
end

function GGE资源:取图像(...)
    local data = self:读数据(...)
    if data then
        return require("SDL.图像")(data,#data)
    end
end

-- function GGE资源:取动画(file)
--     return require("SDL.纹理")(file)
-- end

function GGE资源:取音效(...)
    local data = self:读数据(...)
    if data then
        return require("SDL.音效")(data,#data)
    end
end

function GGE资源:取文字(...)
    local data = self:读数据(...)
    if data then
        return require("SDL.文字")(data,#data)
    end
end

local db3 = require("lsqlite3")

local function prepare_value0(db,sql)
    local vm = db:prepare(sql)
    if vm and vm:step()==db3.ROW then
        return vm:get_value(0)
    end
    return 0
end

GGE资源包 = class("GGE资源包")

function GGE资源包:初始化(file,psd)
    local db = db3.open(file)
    if db then
        if psd then
            local psd = string.format("PRAGMA key = '%s';", psd)
            db:exec(psd)
        end

        local r = prepare_value0(db,"select count(*) from sqlite_master where name='file';")
        if r==0 then
            db:exec[[
                CREATE TABLE "main"."file" (
                    "path"  TEXT,
                    "md5"  TEXT,
                    PRIMARY KEY ("path")
                );

                CREATE UNIQUE INDEX "main"."spath"
                ON "file" ("path" ASC);

                CREATE TABLE "main"."data" (
                    "type"  INTEGER DEFAULT 0,
                    "md5"  TEXT,
                    "time"  INTEGER,
                    "size"  INTEGER,
                    "dsize"  INTEGER,
                    "data"  BLOB,
                    PRIMARY KEY ("md5")
                );

                CREATE UNIQUE INDEX "main"."smd5"
                ON "data" ("md5" ASC);
            ]]
        end
        self._db = db
    end
end

function GGE资源包:是否存在(path)
    if prepare_value0(self._db,string.format("select count(*) from file where path = '%s'; ", path))~=0 then
        return true
    end
    return false
end

function GGE资源包:读数据(path)
    local data = prepare_value0(self._db,string.format("SELECT data FROM data WHERE md5 = (SELECT md5 FROM file WHERE path = '%s');",path))
    return data~=0 and data or nil
end

function GGE资源包:写数据(path,data)

end
return GGE资源