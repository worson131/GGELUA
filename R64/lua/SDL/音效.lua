--[[
    @Author       : GGELUA
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-15 17:24:19
--]]

local _ENV = require("SDL")
MIX_Init()
local SDL音效 = class"SDL音效"

function SDL音效:SDL音效(file)
    self._wav = assert(MIX.LoadWAV(file), GetError(),2)
    if self._wav then
        _mixs[self] = self._wav
    end
end

function SDL音效:播放(loop)
    if type(loop)=='number' then
        self._id = self._wav:PlayChannel(-1,loop)
    else
        self._id = self._wav:PlayChannel(-1,loop and -1 or 0)
    end
    return self
end

function SDL音效:停止()
    MIX.HaltChannel(self._id)
    return self
end

function SDL音效:暂停()
    MIX.Pause(self._id)
    return self
end

function SDL音效:恢复()
    MIX.Resume(self._id)
    return self
end

function SDL音效:是否暂停()
    return MIX.Paused(self._id)
end

function SDL音效:是否停止()
    return not MIX.Playing(self._id)
end

function SDL音效:是否播放()
    return MIX.Playing(self._id)
end

function SDL音效:渐变播放(loop,ms)
    if type(loop)=='number' then
        self._id = self._wav:FadeInChannel(-1,loop,ms)
    else
        self._id = self._wav:FadeInChannel(-1,loop and -1 or 0,ms)
    end
    return self
end

function SDL音效:取渐变状态()
    return MIX.FadingChannel(self._id)
end

function SDL音效:置音量(v,all)
    if self._id then
        MIX.Volume(all and -1 or self._id,v)
        self._wav:VolumeChunk(v)
    end
    return self
end

function SDL音效:取音量()
    if self._id then
        return MIX.Volume(self._id,-1)
    end
    return self._wav:VolumeChunk(-1)
end

return SDL音效