--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-14 22:38:09
--]]
local _ENV = require("SDL")
TTF = require("gsdl2.ttf")
assert(TTF.Init(), GetError(),2)

local SDL文字 = class("SDL文字")

function SDL文字:SDL文字(file,size,aliasing,w)
    self._win  = SDL._win --默认窗口
    self._anti = aliasing ~=false --抗锯齿
    self._w    = w --折行宽度
    self._size = size or 14
    if GetPlatform()=='Windows' then
        local rp = os.getenv("SystemRoot")..'/Fonts/'
        self._font = assert(TTF.OpenFontIndex(file and rp..file or rp..'simsun.ttc', self._size), GetError(),2)
    end
    if self._font then
        _ttfs[self] = self._font
        self:置颜色(255,255,255)
    end
end

function SDL文字:置窗口(v)
    self._win = v
end

function SDL文字:显示(x,y,t)
    if not t and ggetype(x) == 'GGE坐标' then
        t = y
        x,y = x:unpack()
    end
    if self._t ~= t and t and t~='' then
        self._t = t
        self._spr = self:取精灵(t)
        collectgarbage()
    end
    if self._spr then
        self._spr:显示(x,y)
    end
end

function SDL文字:取纹理(t,...)
    if t and t~='' then
        local sf<close> = self:取图像(t,...)
        return self._win:创建纹理(sf)
    end
end

function SDL文字:取精灵(t,...)
    if t and t~='' then
        local sf<close> = self:取图像(t,...)
        return self._win:创建精灵(sf)
    end
    return self._win:创建精灵()
end

function SDL文字:取图像(t,...)
    if t and t~='' then
        if select('#', ...)>0 then
            t = tostring(t):format(...)
        end
        if self._w then--折行
            if self._ba or self._br or self._bg or self._bb then--有背景
                return self._win:创建图像(self._font:RenderUTF8_Shaded_Wrapped(t,self._r,self._g,self._b,self._a,self._br,self._bg,self._bb,self._ba,self._w))
            elseif self._anti then--抗锯齿
                return self._win:创建图像(self._font:RenderUTF8_Blended_Wrapped(t,self._r,self._g,self._b,self._a,self._w))
            else
                return self._win:创建图像(self._font:RenderUTF8_Solid_Wrapped(t,self._r,self._g,self._b,self._a,self._w))
            end
        end
        if self._ba or self._br or self._bg or self._bb then--有背景
            return self._win:创建图像(self._font:RenderUTF8_Shaded(t,self._r,self._g,self._b,self._a,self._br,self._bg,self._bb,self._ba))
        elseif self._anti then--抗锯齿
            return self._win:创建图像(self._font:RenderUTF8_Blended(t,self._r,self._g,self._b,self._a))
        else
            return self._win:创建图像(self._font:RenderUTF8_Solid(t,self._r,self._g,self._b,self._a))
        end
    end
end

--轮廓对宋体无效FT_LOAD_DEFAULT|FT_LOAD_NO_BITMAP
function SDL文字:取描边精灵(t,r,g,b,a)
    if t and t~='' then
        local sfa = self._font:RenderUTF8_Solid(t,self._r,self._g,self._b,self._a)
        local sfb = self._font:RenderUTF8_Solid(t,r or 0,g or 0,b or 0,a)
        local sf = CreateRGBSurfaceWithFormat(sfa.w+2,sfa.h+2)
        local r = CreateRect(1,0,0,0);sfb:UpperBlit(nil,sf,r)
        r:SetRectXY(0,1);sfb:UpperBlit(nil,sf,r)
        r:SetRectXY(2,1);sfb:UpperBlit(nil,sf,r)
        r:SetRectXY(1,2);sfb:UpperBlit(nil,sf,r)
        r:SetRectXY(1,1);sfa:UpperBlit(nil,sf,r)
        return self._win:创建精灵(sf)
    end
    return self._win:创建精灵()
end

function SDL文字:取投影精灵(t,r,g,b,a)
    if t and t~='' then
        local sfa = self._font:RenderUTF8_Solid(t,self._r,self._g,self._b,self._a)
        local sfb = self._font:RenderUTF8_Solid(t,r or 0,g or 0,b or 0,a)
        local sf = CreateRGBSurfaceWithFormat(sfa.w+1,sfa.h+1)
        local r = CreateRect(1,0,0,0);sfb:UpperBlit(nil,sf,r)
        r:SetRectXY(0,1);sfb:UpperBlit(nil,sf,r)
        r:SetRectXY(1,1);sfb:UpperBlit(nil,sf,r)
        r:SetRectXY(0,0);sfa:UpperBlit(nil,sf,r)
        return self._win:创建精灵(sf)
    end
    return self._win:创建精灵()
end

TTF_STYLE_NORMAL        = 0x00--正常
TTF_STYLE_BOLD          = 0x01--粗体
TTF_STYLE_ITALIC        = 0x02--斜体
TTF_STYLE_UNDERLINE     = 0x04--下划线
TTF_STYLE_STRIKETHROUGH = 0x08--删除线
function SDL文字:取样式()
    return self._font:GetFontStyle()
end

function SDL文字:置样式(v)
    self._font:SetFontStyle(v)
    return self
end

function SDL文字:取轮廓()
    return self._font:GetFontOutline()
end

function SDL文字:置轮廓(v)
    self._font:SetFontOutline(v)
    return self
end
TTF_HINTING_NORMAL    = 0
TTF_HINTING_LIGHT     = 1
TTF_HINTING_MONO      = 2
TTF_HINTING_NONE      = 3
TTF_HINTING_LIGHT_SUBPIXEL  =4
function SDL文字:置标志(v)
    self._font:SetFontHinting(v)
    return self
end

function SDL文字:取标志()
    return self._font:GetFontHinting()
end

function SDL文字:置颜色(r,g,b,a)
    self._t = nil
    self._a = a
    self._r = r
    self._g = g
    self._b = b
    return self
end

function SDL文字:取颜色()
    return self._r,self._g,self._b,self._a
end

function SDL文字:置背景颜色(r,g,b,a)
    self._t = nil
    self._ba = a
    self._br = r
    self._bg = g
    self._bb = b
    return self
end

function SDL文字:取背景颜色()
    return self._br,self._bg,self._bb,self._ba
end
--自动折行
function SDL文字:置宽度(w)
    self._t = nil
    self._w = w
    return self
end
--动态修改文字大小
function SDL文字:置大小(v)
    if v~=self._size then
        self._size = v
        self._t = nil
        self._font:SetFontSize(v)
    end
    return self
end

function SDL文字:置抗锯齿(v)
    self._t = nil
    self._anti = v
    return self
end

function SDL文字:取宽度(t)
    return (self._font:SizeUTF8(t))
end

function SDL文字:取高度(t)
    return self._font:FontHeight()
    -- local _,h = self._font:SizeUTF8(s)
    -- return h
end

function SDL文字:取宽高(t)
    return self._font:SizeUTF8(t)
end

-- function SDL文字:Ascent()
--     return self._font:FontAscent()
-- end

-- function SDL文字:Descent()
--     return self._font:FontDescent()
-- end

-- function SDL文字:LineSkip()
--     return self._font:FontLineSkip()
-- end

-- function SDL文字:GetFontKerning()
--     return self._font:GetFontKerning()
-- end

-- function SDL文字:SetFontKerning(v)
--     return self._font:SetFontKerning(v)
-- end

-- function SDL文字:Faces()
--     return self._font:FontFaces()
-- end

-- function SDL文字:FaceIsFixedWidth()
--     return self._font:FontFaceIsFixedWidth()
-- end

-- function SDL文字:取字体名称()
--     return self._font:FontFaceFamilyName()
-- end

-- function SDL文字:取类型名称()
--     return self._font:FontFaceStyleName()
-- end

return SDL文字