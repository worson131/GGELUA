--[[
    @Author       : GGELUA
    @Date         : 2020-01-08 21:52:58
    @LastEditTime : 2021-02-10 17:59:01
--]]

local _ENV = require("SDL")

local SDL纹理 = class"SDL纹理"

local function _载入纹理(rd,a,b,c,d,e)
    if not rd then return end
    if type(a)=='number' and type(b)=='number' then--默认渲染区模式,ARGB8888
        return  assert(rd:CreateTexture(a,b,c,d), GetError(),2)--宽度,高度,模式,像素格式
    elseif type(a)=='string' then--默认可锁定
        if type(b)=='number' and #a==b then--lstring
            local rw = assert(RWFromStr(a), GetError())
            return  assert(rd:LoadTexture_RW(rw,c or TEXTUREACCESS_STREAMING), GetError(),2)
        end
        return assert(rd:LoadTexture(a,b or TEXTUREACCESS_STREAMING), GetError(),2)--文件,模式
    elseif ggetype(a) == 'SDL_Texture' then
        return a
    elseif ggetype(a) == 'SDL_Surface' then
        return assert(rd:CreateTextureFromSurface(a,b or TEXTUREACCESS_STREAMING), GetError(),2)
    elseif ggetype(a) == 'SDL图像' and a:取对象() then
        return assert(rd:CreateTextureFromSurface(a:取对象(),b or TEXTUREACCESS_STREAMING), GetError(),2)
    elseif a~= nil then
        error('未知模式',3)
    end
end

function SDL纹理:SDL纹理(...)
    self._win = SDL._win --默认窗口
    self._tex = _载入纹理(self._win:取渲染器(),...)
    if ggetype(self._tex)=='SDL_Texture' then
        self._win._texs[self] = self._tex
        self.format,self._access,self.宽度,self.高度 = self._tex:QueryTexture()
        if self._access==TEXTUREACCESS_TARGET then
            self._tex:SetTextureBlendMode(BLENDMODE_BLEND)
        end
    end
end

-- function SDL纹理:更新(pixels,pitch)
--     self._tex:UpdateTexture(nil,pixels,pitch)
--     --self._tex:UpdateYUVTexture()
-- end

function SDL纹理:取对象()
    return self._tex
end

function SDL纹理:取模式()
    return self._access
end

function SDL纹理:锁定(x,y,w,h)
    if self._tex then
        if self._access ~= TEXTUREACCESS_STREAMING then
            error('"无法锁定"',2)
        end
        local rect
        if x and y and w and h then
            rect = CreateRect(x,y,w,h)
        end
        return self._tex:LockTexture(rect)
    end
end

function SDL纹理:解锁()
    if self._tex then
        self._tex:UnlockTexture()
    end
    return self
end

function SDL纹理:置过滤(v)
    if self._tex then
        self._tex:SetTextureScaleMode(v)
    end
    return self
end


function SDL纹理:转灰度()
    if self._tex then
        self._tex:TextureToGrayscale()
    end
    return self
end

function SDL纹理:复制区域()
    --return SDL纹理(self._sf:TextureCopyRect())
end

function SDL纹理:到精灵()
    return require("SDL.精灵")(self)
end
return SDL纹理