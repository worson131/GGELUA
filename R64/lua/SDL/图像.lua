--[[
    @Author       : GGELUA
    @Date         : 2020-01-08 21:52:11
    @LastEditTime : 2021-02-18 14:04:37
--]]

require("SDL.精灵")
local _ENV = require("SDL")

local SDL图像 = class"SDL图像"

function SDL图像:SDL图像(a,b,c,d)
    if type(a)=='number' and type(b)=='number' then
        self._sf = assert(CreateRGBSurfaceWithFormat(a,b,c,d), GetError(),2)--宽度，高度，位深，格式
    elseif type(a)=='string' then
        if type(b)=='number' then
            if #a==b then
                local rw = assert(RWFromStr(a), GetError(),2)
                self._sf = assert(IMG.LoadARGB8888_RW(rw), GetError(),2)
            else--透明色
                self._sf = assert(IMG.LoadARGB8888(a,b), GetError(),2)
            end
        else
            self._sf = assert(IMG.LoadARGB8888(a), GetError(),2)
        end
    elseif ggetype(a) == 'SDL_Surface' then
        self._sf = a
    -- elseif ggetype(a) == 'SDL_PixelFormat' then
    end
    if self._sf then
        _sfs[self] = self._sf
        self._win = SDL._win
    end
end

function SDL图像:__close()--即时释放
    if self._sf then
        self._sf:__close()
    end
end

function SDL图像:__index(key)
    if key=='宽度' then
        return self._sf and self._sf.w or 0
    elseif key=='高度' then
        return self._sf and self._sf.h or 0
    end
end

function SDL图像:复制()
    return self._win:创建图像(self._sf:DuplicateSurface())
end

function SDL图像:置窗口(v)
    self._win = v
    return self
end

function SDL图像:显示(x,y)
    if not y and ggetype(x) == 'GGE坐标' then
        x,y = x:unpack()
    end
    if self._win then
        if self._hx then--中心
            x,y = x-self._hx,y-self._hy
        end
        self._x,self._y = x,y
        self._win:显示图像(self._sf,x,y)
    end
end

function SDL图像:取对象()
    return self._sf
end

function SDL图像:锁定()
    if self._sf then
        return self._sf:LockSurface()
    end
end

function SDL图像:解锁()
    return self._sf and self._sf:UnlockSurface()
end

function SDL图像:到灰度()
    self._sf:SurfaceToGrayscale()
    return self
end

function SDL图像:到精灵()
    return require("SDL.精灵")(self)
end

function SDL图像:检查点(x,y)
    local _x,_y = self._x,self._y
    local _w,_h = self._sf.w,self._sf.h
    return (x>=_x) and (x<_x+_w) and (y>=_y) and (y<_y+_h)
end

function SDL图像:检查像素(x,y)
    return self:取像素(x,y)>0
end

function SDL图像:取像素(x,y)
    return self._sf:GetSurfacePixel(x,y)
end

function SDL图像:置像素(x,y,r,g,b,a)
    self._sf:SetSurfacePixel(x,y,r,g,b,a)
    return self
end

function SDL图像:保存文件(file,tp,quality)
    if not tp or tp=='BMP' then
        return self._sf:SaveBMP(file)
    elseif tp=='PNG' then
        return self._sf:SavePNG(file)
    elseif tp=='JPG' then
        return self._sf:SaveJPG(file,quality)
    end
end

function SDL图像:取透明色()
    return self._sf:GetColorKey()
end

function SDL图像:置透明(a)
    self._sf:SetSurfaceBlendMode(BLENDMODE_BLEND)
    self._sf:SetSurfaceAlphaMod(a)
    return self
end

function SDL图像:取透明()
    return self._sf:GetSurfaceBlendMode()
end

function SDL图像:置颜色(r,g,b,a)
    self._sf:SetSurfaceColorMod(r,g,b)
    if a then
        self:置透明(a)
    end
    return self
end

function SDL图像:取颜色()
    return self._sf:GetSurfaceColorMod()
end

function SDL图像:置混合(b)
    self._sf:SetSurfaceBlendMode(b)
    return self
end

function SDL图像:取混合()
    return self._sf:GetSurfaceBlendMode()
end

function SDL图像:置区域(x,y,w,h)
    if x and y and w and h then
        self._sf:SetClipRect(CreateRect(x,y,w,h))
    else
        self._sf:SetClipRect()
    end
    return self
end

function SDL图像:取区域()
    return self._sf:GetClipRect()
end

function SDL图像:复制区域(x,y,w,h)
    return self._win:创建图像(self._sf:GetSurfaceRect(x,y,w,h))
end

function SDL图像:转换(format,flags)
    if type(format)=='number' then
        return self._win:创建图像(assert(self._sf:ConvertSurfaceFormat(format,flags), GetError(),2))
    elseif ggetype(format)=='SDL_PixelFormat' then
        return self._win:创建图像(assert(self._sf:ConvertSurface(format,flags), GetError(),2))
    end
end

function SDL图像:填充(r,g,b,rect)
    return self._sf:FillRect(self._sf.format:MapRGB(r,g,b),rect)
end

function SDL图像:置中心(x,y)
    self._hx,self._hy = math.floor(x),math.floor(y)
    return self
end

function SDL图像:取引用()
    return self._sf.refcount
end

function SDL图像:加引用()
    local refcount = self._sf.refcount
    self._sf.SetSurfaceRef(refcount+1)
    return refcount
end

function SDL图像:减引用()
    local refcount = self._sf.refcount
    if refcount>1 then
        self._sf.SetSurfaceRef(refcount+1)
    end
    return refcount
end
return SDL图像