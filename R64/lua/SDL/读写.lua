--[[
    @Author       : GGELUA
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-15 17:23:57
--]]

local _ENV = require("SDL")
--local _REG = debug.getregistry()
local SDL读写 = class"SDL读写"

function SDL读写:SDL读写(file,mode)
    if type(file)=='string' then
        if type(mode)=='number' and #file==mode then
            self._f = RWFromStr(file,mode)
        else
            self._f = RWFromFile(file,mode)
        end
    -- elseif type(file)=='userdata' then--_REG["FILE*"]
    --     self._f = RWFromFP(file)
    elseif type(file)=='userdata' then
        self._f = RWFromMem(file)
    end
    return self._f
end

-- function SDL读写:__close()
    
-- end

function SDL读写:取大小()
    return self._f:RWsize()
end
RW_SEEK_SET = 0 --从头
RW_SEEK_CUR = 1 --当前
RW_SEEK_END = 2 --从尾
function SDL读写:移动位置(offset,whence)
    return self._f:RWseek(offset,whence)
end

function SDL读写:取位置()
    return self._f:RWtell()
end

function SDL读写:读取(size)
    return self._f:RWread(size)
end

function SDL读写:写入(str)
    return self._f:RWwrite(str)
end

SDL读写.seek  = SDL读写.移动位置
SDL读写.tell  = SDL读写.取位置
SDL读写.read  = SDL读写.读取
SDL读写.write = SDL读写.写入
function SDL读写:读数据(bit,endian)
    if bit==8 then
        return self._f:ReadU8()
    elseif bit==16 then
        return endian and self._f:ReadBE16() or self._f:ReadLE16()
    elseif bit==32 then
        return endian and self._f:ReadBE32() or self._f:ReadLE32()
    elseif bit==64 then
        return endian and self._f:ReadBE64() or self._f:ReadLE64()
    end
end

function SDL读写:写数据(data,bit,endian)
    if bit==8 then
        return self._f:WriteU8(data)
    elseif bit==16 then
        return endian and self._f:WriteBE16(data) or self._f:WriteLE16(data)
    elseif bit==32 then
        return endian and self._f:WriteBE32(data) or self._f:WriteLE32(data)
    elseif bit==64 then
        return endian and self._f:WriteBE64(data) or self._f:WriteLE64(data)
    end
end

return SDL读写