--[[
    @Author       : baidwwy
    @Date         : 2020-01-28 22:40:03
    @LastEditTime : 2021-02-13 21:30:04
--]]
local format = string.format

local _ENV = setmetatable(require"gimgui", {__index=_G})
Init(引擎:取对象())
引擎:注册事件(_ENV)
function 消息事件(_,ev)
    Event(ev)
end

--窗口相关
ImGuiWindowFlags_None                      = 0;
ImGuiWindowFlags_NoTitleBar                = 1 << 0; --没有标题
ImGuiWindowFlags_NoResize                  = 1 << 1; --没有调整
ImGuiWindowFlags_NoMove                    = 1 << 2; --不可移动
ImGuiWindowFlags_NoScrollbar               = 1 << 3; --没有滑块
ImGuiWindowFlags_NoScrollWithMouse         = 1 << 4; 
ImGuiWindowFlags_NoCollapse                = 1 << 5; --没有缩小
ImGuiWindowFlags_AlwaysAutoResize          = 1 << 6; --自动大小
ImGuiWindowFlags_NoBackground              = 1 << 7; --没有背景
ImGuiWindowFlags_NoSavedSettings           = 1 << 8; --不保存ini
ImGuiWindowFlags_NoMouseInputs             = 1 << 9; 
ImGuiWindowFlags_MenuBar                   = 1 << 10;--有菜单
ImGuiWindowFlags_HorizontalScrollbar       = 1 << 11;--水平滑块
ImGuiWindowFlags_NoFocusOnAppearing        = 1 << 12;
ImGuiWindowFlags_NoBringToFrontOnFocus     = 1 << 13;
ImGuiWindowFlags_AlwaysVerticalScrollbar   = 1 << 14;
ImGuiWindowFlags_AlwaysHorizontalScrollbar = 1 << 15;
ImGuiWindowFlags_AlwaysUseWindowPadding    = 1 << 16;
ImGuiWindowFlags_NoNavInputs               = 1 << 18;
ImGuiWindowFlags_NoNavFocus                = 1 << 19;
ImGuiWindowFlags_UnsavedDocument           = 1 << 20;
ImGuiWindowFlags_NoNav                     = ImGuiWindowFlags_NoNavInputs | ImGuiWindowFlags_NoNavFocus;
ImGuiWindowFlags_NoDecoration              = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoCollapse;
ImGuiWindowFlags_NoInputs                  = ImGuiWindowFlags_NoMouseInputs | ImGuiWindowFlags_NoNavInputs | ImGuiWindowFlags_NoNavFocus;
--输入相关
ImGuiInputTextFlags_None                = 0;
ImGuiInputTextFlags_CharsDecimal        = 1 << 0;
ImGuiInputTextFlags_CharsHexadecimal    = 1 << 1;
ImGuiInputTextFlags_CharsUppercase      = 1 << 2;
ImGuiInputTextFlags_CharsNoBlank        = 1 << 3;
ImGuiInputTextFlags_AutoSelectAll       = 1 << 4;
ImGuiInputTextFlags_EnterReturnsTrue    = 1 << 5;
ImGuiInputTextFlags_CallbackCompletion  = 1 << 6;
ImGuiInputTextFlags_CallbackHistory     = 1 << 7;
ImGuiInputTextFlags_CallbackAlways      = 1 << 8;
ImGuiInputTextFlags_CallbackCharFilter  = 1 << 9;
ImGuiInputTextFlags_AllowTabInput       = 1 << 10;
ImGuiInputTextFlags_CtrlEnterForNewLine = 1 << 11;
ImGuiInputTextFlags_NoHorizontalScroll  = 1 << 12;
ImGuiInputTextFlags_AlwaysInsertMode    = 1 << 13;
ImGuiInputTextFlags_ReadOnly            = 1 << 14;--只读
ImGuiInputTextFlags_Password            = 1 << 15;--密码
ImGuiInputTextFlags_NoUndoRedo          = 1 << 16;--禁用撤消/重做
ImGuiInputTextFlags_CharsScientific     = 1 << 17;
ImGuiInputTextFlags_CallbackResize      = 1 << 18;
--树相关
--ImGuiTreeNodeFlags_None                 = 0;
ImGuiTreeNodeFlags_Selected             = 1 << 0; 
ImGuiTreeNodeFlags_Framed               = 1 << 1; 
ImGuiTreeNodeFlags_AllowItemOverlap     = 1 << 2; 
ImGuiTreeNodeFlags_NoTreePushOnOpen     = 1 << 3; 
ImGuiTreeNodeFlags_NoAutoOpenOnLog      = 1 << 4; 
ImGuiTreeNodeFlags_DefaultOpen          = 1 << 5; --默认打开
ImGuiTreeNodeFlags_OpenOnDoubleClick    = 1 << 6; --双击才能打开
ImGuiTreeNodeFlags_OpenOnArrow          = 1 << 7; --点击箭头打开
ImGuiTreeNodeFlags_Leaf                 = 1 << 8; --无折叠，无箭头
ImGuiTreeNodeFlags_Bullet               = 1 << 9; --圆点符号
ImGuiTreeNodeFlags_FramePadding         = 1 << 10;
ImGuiTreeNodeFlags_SpanAvailWidth       = 1 << 11;
ImGuiTreeNodeFlags_SpanFullWidth        = 1 << 12;
ImGuiTreeNodeFlags_NavLeftJumpsBackHere = 1 << 13;
ImGuiTreeNodeFlags_CollapsingHeader     = ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_NoAutoOpenOnLog
--选择相关
--ImGuiSelectableFlags_None               = 0
ImGuiSelectableFlags_DontClosePopups    = 1 << 0
ImGuiSelectableFlags_SpanAllColumns     = 1 << 1--多列 整行选择
ImGuiSelectableFlags_AllowDoubleClick   = 1 << 2--允许双击IsMouseDoubleClicked
ImGuiSelectableFlags_Disabled           = 1 << 3--禁止
ImGuiSelectableFlags_AllowItemOverlap   = 1 << 4
--组合相关
--ImGuiComboFlags_None                    = 0
ImGuiComboFlags_PopupAlignLeft          = 1 << 0
ImGuiComboFlags_HeightSmall             = 1 << 1
ImGuiComboFlags_HeightRegular           = 1 << 2
ImGuiComboFlags_HeightLarge             = 1 << 3
ImGuiComboFlags_HeightLargest           = 1 << 4
ImGuiComboFlags_NoArrowButton           = 1 << 5
ImGuiComboFlags_NoPreview               = 1 << 6
ImGuiComboFlags_HeightMask_             = ImGuiComboFlags_HeightSmall | ImGuiComboFlags_HeightRegular | ImGuiComboFlags_HeightLarge | ImGuiComboFlags_HeightLargest
--标签相关
--ImGuiTabBarFlags_None                           = 0
ImGuiTabBarFlags_Reorderable                    = 1 << 0 --可以拖动
ImGuiTabBarFlags_AutoSelectNewTabs              = 1 << 1--选中新标签
ImGuiTabBarFlags_TabListPopupButton             = 1 << 2
ImGuiTabBarFlags_NoCloseWithMiddleMouseButton   = 1 << 3
ImGuiTabBarFlags_NoTabListScrollingButtons      = 1 << 4
ImGuiTabBarFlags_NoTooltip                      = 1 << 5--没有提示
ImGuiTabBarFlags_FittingPolicyResizeDown        = 1 << 6
ImGuiTabBarFlags_FittingPolicyScroll            = 1 << 7
ImGuiTabBarFlags_FittingPolicyMask_             = ImGuiTabBarFlags_FittingPolicyResizeDown | ImGuiTabBarFlags_FittingPolicyScroll;
ImGuiTabBarFlags_FittingPolicyDefault_          = ImGuiTabBarFlags_FittingPolicyResizeDown

--ImGuiTabItemFlags_None                          = 0
ImGuiTabItemFlags_UnsavedDocument               = 1 << 0
ImGuiTabItemFlags_SetSelected                   = 1 << 1 --选中
ImGuiTabItemFlags_NoCloseWithMiddleMouseButton  = 1 << 2
ImGuiTabItemFlags_NoPushId                      = 1 << 3

-- --ImGuiFocusedFlags_None                          = 0,
-- ImGuiFocusedFlags_ChildWindows                  = 1 << 0
-- ImGuiFocusedFlags_RootWindow                    = 1 << 1
-- ImGuiFocusedFlags_AnyWindow                     = 1 << 2
-- ImGuiFocusedFlags_RootAndChildWindows           = ImGuiFocusedFlags_RootWindow | ImGuiFocusedFlags_ChildWindows

-- --ImGuiHoveredFlags_None                          = 0,    
-- ImGuiHoveredFlags_ChildWindows                  = 1 << 0
-- ImGuiHoveredFlags_RootWindow                    = 1 << 1
-- ImGuiHoveredFlags_AnyWindow                     = 1 << 2
-- ImGuiHoveredFlags_AllowWhenBlockedByPopup       = 1 << 3
-- ImGuiHoveredFlags_AllowWhenBlockedByActiveItem  = 1 << 5
-- ImGuiHoveredFlags_AllowWhenOverlapped           = 1 << 6
-- ImGuiHoveredFlags_AllowWhenDisabled             = 1 << 7
-- ImGuiHoveredFlags_RectOnly                      = ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenOverlapped,
-- ImGuiHoveredFlags_RootAndChildWindows           = ImGuiHoveredFlags_RootWindow | ImGuiHoveredFlags_ChildWindows

-- --ImGuiDragDropFlags_None                         = 0,
-- ImGuiDragDropFlags_SourceNoPreviewTooltip       = 1 << 0
-- ImGuiDragDropFlags_SourceNoDisableHover         = 1 << 1
-- ImGuiDragDropFlags_SourceNoHoldToOpenOthers     = 1 << 2
-- ImGuiDragDropFlags_SourceAllowNullID            = 1 << 3
-- ImGuiDragDropFlags_SourceExtern                 = 1 << 4
-- ImGuiDragDropFlags_SourceAutoExpirePayload      = 1 << 5
-- ImGuiDragDropFlags_AcceptBeforeDelivery         = 1 << 1
-- ImGuiDragDropFlags_AcceptNoDrawDefaultRect      = 1 << 1
-- ImGuiDragDropFlags_AcceptNoPreviewTooltip       = 1 << 1
-- ImGuiDragDropFlags_AcceptPeekOnly               = ImGuiDragDropFlags_AcceptBeforeDelivery | ImGuiDragDropFlags_AcceptNoDrawDefaultRect  // For peeking ahead and inspecting the payload before delivery.

--样式相关
ImGuiCol_Text                  = 0
ImGuiCol_TextDisabled          = 1
ImGuiCol_WindowBg              = 2
ImGuiCol_ChildBg               = 3
ImGuiCol_PopupBg               = 4
ImGuiCol_Border                = 5
ImGuiCol_BorderShadow          = 6
ImGuiCol_FrameBg               = 7
ImGuiCol_FrameBgHovered        = 8
ImGuiCol_FrameBgActive         = 9
ImGuiCol_TitleBg               = 10
ImGuiCol_TitleBgActive         = 11
ImGuiCol_TitleBgCollapsed      = 12
ImGuiCol_MenuBarBg             = 13
ImGuiCol_ScrollbarBg           = 14
ImGuiCol_ScrollbarGrab         = 15
ImGuiCol_ScrollbarGrabHovered  = 16
ImGuiCol_ScrollbarGrabActive   = 17
ImGuiCol_CheckMark             = 18
ImGuiCol_SliderGrab            = 19
ImGuiCol_SliderGrabActive      = 20
ImGuiCol_Button                = 21
ImGuiCol_ButtonHovered         = 22
ImGuiCol_ButtonActive          = 23
ImGuiCol_Header                = 24
ImGuiCol_HeaderHovered         = 25
ImGuiCol_HeaderActive          = 26
ImGuiCol_Separator             = 27
ImGuiCol_SeparatorHovered      = 28
ImGuiCol_SeparatorActive       = 29
ImGuiCol_ResizeGrip            = 30
ImGuiCol_ResizeGripHovered     = 31
ImGuiCol_ResizeGripActive      = 32
ImGuiCol_Tab                   = 33
ImGuiCol_TabHovered            = 34
ImGuiCol_TabActive             = 35
ImGuiCol_TabUnfocused          = 36
ImGuiCol_TabUnfocusedActive    = 37
ImGuiCol_PlotLines             = 38
ImGuiCol_PlotLinesHovered      = 39
ImGuiCol_PlotHistogram         = 40
ImGuiCol_PlotHistogramHovered  = 41
ImGuiCol_TextSelectedBg        = 42
ImGuiCol_DragDropTarget        = 43
ImGuiCol_NavHighlight          = 44
ImGuiCol_NavWindowingHighlight = 45
ImGuiCol_NavWindowingDimBg     = 46
ImGuiCol_ModalWindowDimBg      = 47

ImGuiStyleVar_Alpha               = 0--透明度
ImGuiStyleVar_WindowPadding       = 1--间隔
ImGuiStyleVar_WindowRounding      = 2--窗口圆角
ImGuiStyleVar_WindowBorderSize    = 3--窗口边框尺寸
ImGuiStyleVar_WindowMinSize       = 4--窗口最小尺寸
ImGuiStyleVar_WindowTitleAlign    = 5--窗口标题对齐
ImGuiStyleVar_ChildRounding       = 6 --子圆角
ImGuiStyleVar_ChildBorderSize     = 7--子边框尺寸
ImGuiStyleVar_PopupRounding       = 8
ImGuiStyleVar_PopupBorderSize     = 9
ImGuiStyleVar_FramePadding        = 10
ImGuiStyleVar_FrameRounding       = 11
ImGuiStyleVar_FrameBorderSize     = 12
ImGuiStyleVar_ItemSpacing         = 13
ImGuiStyleVar_ItemInnerSpacing    = 14
ImGuiStyleVar_IndentSpacing       = 15
ImGuiStyleVar_ScrollbarSize       = 16
ImGuiStyleVar_ScrollbarRounding   = 17
ImGuiStyleVar_GrabMinSize         = 18
ImGuiStyleVar_GrabRounding        = 19
ImGuiStyleVar_TabRounding         = 20
ImGuiStyleVar_ButtonTextAlign     = 21
ImGuiStyleVar_SelectableTextAlign = 22

ImGuiCond_Always        = 1 << 0 --永久
ImGuiCond_Once          = 1 << 1 --仅一次
ImGuiCond_FirstUseEver  = 1 << 2 --首次
ImGuiCond_Appearing     = 1 << 3 --显示

ImGuiMouseButton_Left   = 0
ImGuiMouseButton_Right  = 1
ImGuiMouseButton_Middle = 2


ImGuiDir_None    = -1
ImGuiDir_Left    = 0
ImGuiDir_Right   = 1
ImGuiDir_Up      = 2
ImGuiDir_Down    = 3


显示     = Render
开始渲染 = NewFrame
结束渲染 = EndFrame


function Text(f, ...)
    f = tostring(f)
    if select("#", ...)>0 then
        f = format(f, ...)
    end
    TextUnformatted(f)
end

开始窗口 = Begin --名称,{true},ImGuiWindowFlags_
结束窗口 = End
开始子窗口 = BeginChild --名称,尺寸，是否有边框，标示
结束子窗口 = EndChild
--IsWindowAppearing
--IsWindowCollapsed
--IsWindowFocused
--GetWindowDrawList
取窗口坐标 = GetWindowPos
取窗口大小 = GetWindowSize
取窗口宽度 = GetWindowWidth
取窗口高度 = GetWindowHeight

置下一个窗口坐标 = SetNextWindowPos  --x,y,ImGuiCond_,窗口中心x,窗口中心y
置下一个窗口大小 = SetNextWindowSize --w,h,ImGuiCond_
--SetNextWindowSizeConstraints
--SetNextWindowContentSize
--SetNextWindowCollapsed
--SetNextWindowFocus
置下一个窗口透明度 = SetNextWindowBgAlpha

置窗口坐标 = SetWindowPos
置窗口大小 = SetWindowSize
置窗口卷缩 = SetWindowCollapsed  --true,ImGuiCond_
置窗口焦点 = SetWindowFocus
--SetWindowFontScale

--GetContentRegionMax
--滚动条相关
--GetScrollX

--PushFont

设置项目宽度 = PushItemWidth
取消项目宽度 = PopItemWidth
置下一个项目宽度 = SetNextItemWidth
取项目宽度 = CalcItemWidth

分割线 = Separator
同一行 = SameLine
新一行 = NewLine
垂直间距 = Spacing
空白 = Dummy
右移 = Indent
左移 = Unindent
对齐文本 = AlignTextToFramePadding


设置ID = PushID
取消ID = PopID

如果窗口悬停 = IsWindowHovered
--文本相关
    文本 = Text --格式化
    颜色文本 = TextColored --{0,0,0,0},t
    禁止文本 = TextDisabled
    换行文本 = TextWrapped
    标签文本 = LabelText
    标点文本 = BulletText
    直接文本 = TextUnformatted
--按钮相关
    按钮 = Button
    小按钮 = SmallButton
    无形按钮 = InvisibleButton
    箭头按钮 = ArrowButton --ImGuiDir_
local image = Image
function Image(tex,...)
    if ggetype(tex)=='SDL纹理' then
        tex = tex:取对象()
    elseif ggetype(tex)=='SDL精灵' then
        tex = tex:取纹理()
    elseif ggetype(tex)~='SDL_Texture' then
        tex = nil
    end
    if tex then
        image(tex,...)
    end
end
    纹理 = Image
    纹理按钮 = ImageButton
    复选按钮 = Checkbox --名称，开关
    --CheckboxFlags
    单选按钮 = RadioButton
    进度条 = ProgressBar
    标点 = Bullet
--组合相关
    开始组合框 = BeginCombo
    结束组合框 = EndCombo
    组合框 = Combo --名称，选中，表，标识
                   --名称，选中，文本，标识
--拖拉相关
--DragInt
--DragFloat
--滑块相关
--SliderFloat
--输入相关
    申请缓存 = newbuffer
    输入框 = InputText --ImGuiInputTextFlags_
    多行输入框 = InputTextMultiline
    --InputTextWithHint
    浮点输入框 = InputFloat
    整数输入框 = InputInt
    --InputDouble
    --InputScalar
--颜色相关
颜色选择 = ColorEdit3
--ColorPicker3
--SetColorEditOptions
--树相关
树节点 = TreeNode
--TreeNodeEx

--CollapsingHeader
--SetNextItemOpen

可选项 = Selectable
--列表相关
列表框 = ListBox
--BeginListBox
--EndListBox
--数据图表
--PlotLines
到文本 = Value
--菜单相关
    开始菜单栏 = BeginMenuBar
    结束菜单栏 = EndMenuBar
    开始主菜单栏 = BeginMainMenuBar
    结束主菜单栏 = EndMainMenuBar
    开始菜单 = BeginMenu --名称，禁止
    结束菜单 = EndMenu
    菜单项目 = MenuItem  --快捷键,选中,禁止
--提示相关
    置提示 = SetTooltip
    开始提示 = BeginTooltip
    结束提示 = EndTooltip
--弹出相关
    打开弹出 = OpenPopup
    开始弹出 = BeginPopup
    开始弹出项目 = BeginPopupContextItem --右键列表
    开始弹出窗口 = BeginPopupContextWindow
    --BeginPopupContextVoid
    开始弹出模态 = BeginPopupModal --模态窗口
    结束弹出 = EndPopup
    项目单击弹出 = OpenPopupOnItemClick
    是否弹出 = IsPopupOpen
    关闭弹出 = CloseCurrentPopup
--多列表相关
    多列表 = Columns --数量，名称，边框
    下一列 = NextColumn
    取列表位置 = GetColumnIndex
    取列表宽度 = GetColumnWidth
    置列表宽度 = SetColumnWidth --位置(-1当前)，宽度
    取列表偏移 = GetColumnOffset
    置列表偏移 = SetColumnOffset
    取列表数量 = GetColumnsCount
--标签相关
    开始标签栏 = BeginTabBar --名称，标示
    结束菜单栏 = EndTabBar
    开始标签项目 = BeginTabItem -- 名称，开关，标示
                                --名称，标示
    结束标签项目 = EndTabItem
--SetTabItemClosed
--日志
--LogToTTY
--拖放
--BeginDragDropSource
--裁剪
--PushClipRect
--项目相关
    置默认焦点 = SetItemDefaultFocus
    如果项目悬停 = IsItemHovered
    --IsItemActive
    --IsItemFocused
    如果项目点击 = IsItemClicked
    --IsItemVisible
    --IsItemEdited
    --IsItemActivated
    --IsItemDeactivated
    --IsItemDeactivatedAfterEdit
    --IsItemToggledOpen
    --IsAnyItemHovered
    --IsAnyItemActive
    --IsAnyItemFocused
    --GetItemRectMin
    --GetItemRectMax
    --GetItemRectSize
    --SetItemAllowOverlap
--其它
--IsRectVisible
--颜色相关
--输入相关
--GetKeyIndex
--IsKeyDown
是否双击 = IsMouseDoubleClicked --ImGuiMouseButton_

取剪贴板 = GetClipboardText
置剪贴板 = SetClipboardText


置样式 = PushStyleVar

开始群组 = BeginGroup
结束群组 = EndGroup
return _ENV